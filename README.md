This project is for the _Fabric_ version of _Linking Books_. For the _Forge_ version, please see https://gitlab.com/TheFloydman/linking-books.

--------------------

Builds hosted at CurseForge: https://www.curseforge.com/minecraft/mc-mods/linkingbooks

--------------------

_Myst_ has come to _Minecraft_ 1.16 with **_Linking Books_**! It is currently in beta and requires at least _Fabric_ 0.9.3. Additional dependencies are:
- Fabric API 0.28.4+1.16
- Cardinal Components API 2.7.10
- Mod Menu 1.14.13
- Cloth Config API 4.8.3
- Auto Config API 3.3.1

This is not meant to be a replacement for _Mystcraft 2_. Dynamic dimension creation is outside the scope of this mod.

--------------------

Need help or just want to chat? Join us on Discord at https://discord.gg/eMSubn6tRt.
<br><a href="https://discord.gg/eMSubn6tRt"><img src="https://discordapp.com/assets/fc0b01fe10a0b8c602fb0106d8189d9b.png" width="200" height="68" /></a>

--------------------

This mod adds special books that allow you to teleport to the exact location where they were first activated. A linking book has no use when it is blank. To activate a blank linking book, simply use it while holding it. It will then be irreversibly linked to the coordinates it was activated at. Per Myst canon, linking books cannot be used within the same dimension ("Age") they lead to. They also do not travel with you. If you teleport with a written linking book while it is in your hand, it will drop to the ground as an entity. Linking Books also adds a Linking Lectern block for displaying and protecting your Linking Books.

**There are several ways to use a written linking book and bring up its GUI:**

- Use a written linking book when it is in your hand.
- Right-click a linking lectern that currently holds a written linking book.
- Right-click a written linking book that is lying on the ground.

--------------------

**Planned Features:**

- Fleshed-out ink system that will make creating Linking Panels a bit more involved.
- Linking Panels with special effects like intra-dimension teleportation and momentum preservation. The building blocks for this feature are already in place.
- Miscellaneous decorative blocks from the Myst universe.
