/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.screen;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.DyeColor;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.integration.ImmersivePortalsIntegration;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.util.ColorUtils;
import thefloydman.linkingbooks.util.Reference;

public class LinkingBookScreenHandler extends ScreenHandler {

    public boolean holdingBook = false;
    public int bookColor = ColorUtils.dyeColorAsInt(DyeColor.GREEN);
    public LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
            .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
    public boolean canLink = false;
    public CompoundTag linkingPanelImage = new CompoundTag();

    public LinkingBookScreenHandler(int windowId, PlayerInventory playerInventory) {
        super(ModScreenHandlerTypes.LINKING_BOOK, windowId);
    }

    public LinkingBookScreenHandler(int windowId, PlayerInventory playerInventory, PacketByteBuf extraData) {
        this(windowId, playerInventory);
        if (extraData.isReadable())
            this.holdingBook = extraData.readBoolean();
        if (extraData.isReadable())
            this.bookColor = extraData.readInt();
        if (extraData.isReadable())
            this.linkData.readFromPacketByteBuf(extraData);
        if (extraData.isReadable())
            this.canLink = extraData.readBoolean();
        if (extraData.isReadable())
            this.linkingPanelImage = extraData.readCompoundTag();
        if (Reference.isImmersivePortalsLoaded() && !playerInventory.player.getEntityWorld().isClient()
                && this.canLink) {
            ImmersivePortalsIntegration.addChunkLoader(this.linkData, (ServerPlayerEntity) playerInventory.player);
        }
    }

    public LinkingBookScreenHandler(int windowId, PlayerInventory playerInventory, boolean holdingBook,
            ItemStack bookStack, boolean canLink, CompoundTag linkingPanelImage) {
        this(windowId, playerInventory);
        this.holdingBook = holdingBook;
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
        ColorComponent colorData = ModComponents.ITEM_COLOR.get(bookStack);
        this.bookColor = colorData.getColor();
        this.linkData = linkData;
        this.canLink = canLink;
        this.linkingPanelImage = linkingPanelImage;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void close(PlayerEntity player) {
        if (Reference.isImmersivePortalsLoaded() && !player.getEntityWorld().isClient() && this.canLink) {
            ImmersivePortalsIntegration.removeChunkLoader(this.linkData, (ServerPlayerEntity) player);
        }
        super.close(player);
    }

}
