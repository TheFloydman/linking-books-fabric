/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.linking;

import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.util.Reference.LinkEffectNames;
import thefloydman.linkingbooks.util.registry.ModRegistries;

public class LinkEffects {

    public static final LinkEffect DUMMY = new DummyLinkEffect();
    public static final LinkEffect POISON_EFFECT = new PotionLinkEffect(StatusEffects.POISON, 20 * 10);
    public static final LinkEffect INTRAAGE_LINKING = new IntraAgeLinkingLinkEffect();
    public static final LinkEffect TETHERED = new TetheredLinkEffect();

    public static void register() {
        Registry.register(ModRegistries.LINK_EFFECTS, Reference.getAsResourceLocation("dummy"), DUMMY);
        Registry.register(ModRegistries.LINK_EFFECTS, LinkEffectNames.POISON_EFFECT, POISON_EFFECT);
        Registry.register(ModRegistries.LINK_EFFECTS, LinkEffectNames.INTRAAGE_LINKING, INTRAAGE_LINKING);
        Registry.register(ModRegistries.LINK_EFFECTS, LinkEffectNames.TETHERED, TETHERED);
    }

}
