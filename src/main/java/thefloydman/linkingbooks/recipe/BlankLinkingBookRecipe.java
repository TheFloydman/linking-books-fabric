/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.recipe;

import java.awt.Color;
import java.util.stream.Stream;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeFinder;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.mixin.IngredientMixin;

public class BlankLinkingBookRecipe implements CraftingRecipe {

    private final Identifier id;
    private final ItemStack recipeOutput;
    private final DefaultedList<Ingredient> recipeInputs;

    public BlankLinkingBookRecipe(Identifier id, ItemStack recipeOutput, DefaultedList<Ingredient> recipeInput) {
        this.recipeOutput = recipeOutput;
        this.recipeInputs = recipeInput;
        this.id = id;
    }

    public static class Serializer implements RecipeSerializer<BlankLinkingBookRecipe> {

        @Override
        public BlankLinkingBookRecipe read(Identifier id, JsonObject json) {
            DefaultedList<Ingredient> ingredients = readIngredients(JsonHelper.getArray(json, "ingredients"));
            if (ingredients.isEmpty()) {
                throw new JsonParseException("No ingredients for blank linking book recipe");
            } else if (ingredients.size() > 3 * 3) {
                throw new JsonParseException(
                        "Too many ingredients for blank linking book recipe. The max is " + (3 * 3));
            } else {
                float red = 0.0F;
                float green = 0.0F;
                float blue = 0.0F;
                if (json.has("color") && json.get("color").isJsonObject()) {
                    JsonObject color = json.getAsJsonObject("color");
                    if (color.has("red") && color.get("red").isJsonPrimitive()) {
                        red = color.get("red").getAsFloat();
                    }
                    if (color.has("green") && color.get("green").isJsonPrimitive()) {
                        green = color.get("green").getAsFloat();
                    }
                    if (color.has("blue") && color.get("blue").isJsonPrimitive()) {
                        blue = color.get("blue").getAsFloat();
                    }
                }
                ItemStack stack = ModItems.BLANK_LINKING_BOOK.getDefaultStack();
                ColorComponent color = ModComponents.ITEM_COLOR.get(stack);
                color.setColor(new Color(red, green, blue).getRGB());
                return new BlankLinkingBookRecipe(id, stack, ingredients);
            }
        }

        @Override
        public BlankLinkingBookRecipe read(Identifier id, PacketByteBuf buffer) {
            int i = buffer.readVarInt();
            DefaultedList<Ingredient> ingredients = DefaultedList.ofSize(i, Ingredient.EMPTY);

            for (int j = 0; j < ingredients.size(); ++j) {
                ingredients.set(j, Ingredient.fromPacket(buffer));
            }

            ItemStack stack = buffer.readItemStack();
            return new BlankLinkingBookRecipe(id, stack, ingredients);
        }

        @Override
        public void write(PacketByteBuf buffer, BlankLinkingBookRecipe recipe) {
            buffer.writeVarInt(recipe.recipeInputs.size());

            for (Ingredient ingredient : recipe.recipeInputs) {
                ingredient.write(buffer);
            }

            buffer.writeItemStack(recipe.recipeOutput);
        }
    }

    private static DefaultedList<Ingredient> readIngredients(JsonArray array) {
        DefaultedList<Ingredient> ingredients = DefaultedList.of();

        for (int i = 0; i < array.size(); ++i) {
            Ingredient ingredient = Ingredient.fromJson(array.get(i));
            if (!ingredient.isEmpty()) {
                ingredients.add(ingredient);
            }
        }

        return ingredients;
    }

    @Override
    public boolean matches(CraftingInventory inventory, World world) {
        RecipeFinder recipeFinder = new RecipeFinder();
        int i = 0;

        // Determines how many non-empty stacks are in the crafting grid. Also places
        // non-empty stacks into a DefaultedList.
        DefaultedList<Ingredient> craftingInputs = DefaultedList.of();
        for (int j = 0; j < inventory.size(); ++j) {
            ItemStack itemStack = inventory.getStack(j);
            if (!itemStack.isEmpty()) {
                ++i;
                recipeFinder.method_20478(itemStack, 1);
                craftingInputs.add(Ingredient.ofStacks(Stream.of(inventory.getStack(j))));
            }
        }

        // Checks if the previously created DefaultedList exactly matches the inputs for
        // this recipe.
        boolean matches = true;
        for (int j = 0; j < craftingInputs.size(); j++) {
            boolean foundMatch = false;
            for (int k = 0; k < this.recipeInputs.size(); k++) {
                // These mixins help make certain fields public.
                ((IngredientMixin) (Object) this.recipeInputs.get(k)).invokeCacheMatchingStacks();
                ItemStack[] stacks = ((IngredientMixin) (Object) this.recipeInputs.get(k)).getMatchingStacks();
                for (ItemStack stack : stacks) {
                    ((IngredientMixin) (Object) craftingInputs.get(j)).invokeCacheMatchingStacks();
                    ItemStack[] matchingStacks = ((IngredientMixin) (Object) craftingInputs.get(j)).getMatchingStacks();
                    if (matchingStacks == null) {
                        continue;
                    }
                    if (stack.getItem() == matchingStacks[0].getItem()) {
                        foundMatch = true;
                        break;
                    }
                }

            }
            if (!foundMatch) {
                matches = false;
                break;
            }
        }
        return i == this.recipeInputs.size() && matches;
    }

    @Override
    public ItemStack craft(CraftingInventory inv) {
        return this.recipeOutput.copy();
    }

    @Override
    public boolean fits(int width, int height) {
        return width * height >= this.recipeInputs.size();
    }

    @Override
    public ItemStack getOutput() {
        return this.recipeOutput;
    }

    @Override
    public Identifier getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return ModRecipeSerializers.BLANK_LINKING_BOOK;
    }
}
