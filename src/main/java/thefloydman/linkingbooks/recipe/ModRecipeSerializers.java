/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.recipe;

import net.minecraft.recipe.RecipeSerializer;
import thefloydman.linkingbooks.util.Reference.RecipeSerializerNames;

public class ModRecipeSerializers {

    public static final RecipeSerializer<BlankLinkingBookRecipe> BLANK_LINKING_BOOK = RecipeSerializer
            .register(RecipeSerializerNames.BLANK_LINKING_BOOK, new BlankLinkingBookRecipe.Serializer());

    public static final RecipeSerializer<LinkEffectRecipe> LINK_EFFECT = RecipeSerializer
            .register(RecipeSerializerNames.LINK_EFFECT, new LinkEffectRecipe.Serializer());

    public static void register() {
    }
}
