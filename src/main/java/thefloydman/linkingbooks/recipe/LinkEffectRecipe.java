/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.recipe;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeFinder;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.mixin.IngredientMixin;
import thefloydman.linkingbooks.util.registry.ModRegistries;

public class LinkEffectRecipe implements CraftingRecipe {

    private final Identifier id;
    private final DefaultedList<Ingredient> recipeInputs;
    private Set<LinkEffect> linkEffects = new HashSet<LinkEffect>();

    public LinkEffectRecipe(Identifier id, DefaultedList<Ingredient> recipeInput, Set<LinkEffect> linkEffects) {
        this.linkEffects = linkEffects;
        this.recipeInputs = recipeInput;
        this.id = id;
    }

    public static class Serializer implements RecipeSerializer<LinkEffectRecipe> {

        @Override
        public LinkEffectRecipe read(Identifier id, JsonObject json) {
            DefaultedList<Ingredient> ingredients = readIngredients(
                    JsonHelper.getArray(json, "additional_ingredients"));
            if (ingredients.size() > 8) {
                throw new JsonParseException(
                        "Too many additional ingredients for written linking book recipe. The max is 8");
            } else {
                Set<LinkEffect> linkEffects = new HashSet<LinkEffect>();
                JsonArray jsonArray = JsonHelper.getArray(json, "link_effects");
                for (JsonElement element : jsonArray) {
                    linkEffects.add(LinkEffect.get(new Identifier(element.getAsString())));
                }
                return new LinkEffectRecipe(id, ingredients, linkEffects);
            }
        }

        @Override
        public LinkEffectRecipe read(Identifier id, PacketByteBuf buffer) {
            int i = buffer.readVarInt();
            DefaultedList<Ingredient> ingredients = DefaultedList.ofSize(i, Ingredient.EMPTY);

            for (int j = 0; j < ingredients.size(); ++j) {
                ingredients.set(j, Ingredient.fromPacket(buffer));
            }

            Set<LinkEffect> linkEffects = new HashSet<LinkEffect>();
            int quantity = buffer.readInt();
            for (int j = 0; j < quantity; j++) {
                linkEffects.add(LinkEffect.get(new Identifier(buffer.readString())));
            }

            return new LinkEffectRecipe(id, ingredients, linkEffects);
        }

        @Override
        public void write(PacketByteBuf buffer, LinkEffectRecipe recipe) {
            buffer.writeVarInt(recipe.recipeInputs.size());

            for (Ingredient ingredient : recipe.recipeInputs) {
                ingredient.write(buffer);
            }

            buffer.writeInt(recipe.linkEffects.size());
            for (LinkEffect effect : recipe.linkEffects) {
                buffer.writeString(ModRegistries.LINK_EFFECTS.getId(effect).toString());
            }
        }
    }

    private static DefaultedList<Ingredient> readIngredients(JsonArray array) {
        DefaultedList<Ingredient> ingredients = DefaultedList.of();

        for (int i = 0; i < array.size(); ++i) {
            Ingredient ingredient = Ingredient.fromJson(array.get(i));
            if (!ingredient.isEmpty()) {
                ingredients.add(ingredient);
            }
        }

        return ingredients;
    }

    @Override
    public boolean matches(CraftingInventory inventory, World world) {
        RecipeFinder recipeFinder = new RecipeFinder();
        int i = 0;

        // Determines how many non-empty stacks are in the crafting grid. Also places
        // non-empty stacks into a DefaultedList.
        DefaultedList<Ingredient> craftingInputs = DefaultedList.of();
        for (int j = 0; j < inventory.size(); ++j) {
            ItemStack itemStack = inventory.getStack(j);
            if (!itemStack.isEmpty()) {
                ++i;
                recipeFinder.method_20478(itemStack, 1);
                craftingInputs.add(Ingredient.ofStacks(Stream.of(inventory.getStack(j))));
            }
        }

        // Checks if the previously created DefaultedList exactly matches the inputs for
        // this recipe.
        boolean matches = true;
        for (int j = 0; j < craftingInputs.size(); j++) {
            boolean foundMatch = false;
            DefaultedList<Ingredient> allInputs = DefaultedList.of();
            for (Ingredient ingredient : this.recipeInputs) {
                allInputs.add(ingredient);
            }
            for (int k = 0; k < this.recipeInputs.size(); k++) {
                // These mixins help make certain fields public.
                ((IngredientMixin) (Object) this.recipeInputs.get(k)).invokeCacheMatchingStacks();
                ItemStack[] stacks = ((IngredientMixin) (Object) this.recipeInputs.get(k)).getMatchingStacks();
                for (ItemStack stack : stacks) {
                    ((IngredientMixin) (Object) craftingInputs.get(j)).invokeCacheMatchingStacks();
                    ItemStack[] matchingStacks = ((IngredientMixin) (Object) craftingInputs.get(j)).getMatchingStacks();
                    if (matchingStacks == null) {
                        continue;
                    }
                    if (stack.getItem() == matchingStacks[0].getItem()
                            || matchingStacks[0].getItem() instanceof WrittenLinkingBookItem) {
                        foundMatch = true;
                        break;
                    }
                }

            }
            if (!foundMatch) {
                matches = false;
                break;
            }
        }
        return i == this.recipeInputs.size() + 1 && matches;
    }

    @Override
    public ItemStack craft(CraftingInventory inv) {
        ItemStack writtenBook = ItemStack.EMPTY;
        for (int i = 0; i < inv.size(); i++) {
            if (inv.getStack(i).getItem() instanceof WrittenLinkingBookItem) {
                writtenBook = inv.getStack(i).copy();
                break;
            }
        }
        if (!writtenBook.isEmpty()) {
            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(writtenBook);
            for (LinkEffect effect : this.linkEffects) {
                if (!linkData.addLinkEffect(effect)) {
                    return ItemStack.EMPTY;
                }
            }
        }
        return writtenBook;
    }

    @Override
    public boolean fits(int width, int height) {
        return width * height >= this.recipeInputs.size();
    }

    @Override
    public ItemStack getOutput() {
        return ModItems.WRITTEN_LINKING_BOOK.getDefaultStack();
    }

    @Override
    public Identifier getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return ModRecipeSerializers.LINK_EFFECT;
    }
}
