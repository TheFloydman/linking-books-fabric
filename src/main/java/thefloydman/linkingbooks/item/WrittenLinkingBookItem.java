/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.item;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.fabricmc.fabric.impl.screenhandler.Networking;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.screen.LinkingBookScreenHandler;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.util.registry.ModRegistries;
import thefloydman.linkingbooks.world.LinkingBooksPersistentState;

public class WrittenLinkingBookItem extends LinkingBookItem implements ExtendedScreenHandlerFactory {

    public WrittenLinkingBookItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack heldStack = player.getStackInHand(hand);
        if (hand == Hand.MAIN_HAND) {
            if (!world.isClient() && !player.isSneaking()) {
                PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
                this.writeScreenOpeningData((ServerPlayerEntity) player, buf);
                Networking.sendOpenPacket((ServerPlayerEntity) player, this,
                        new LinkingBookScreenHandler(0, player.inventory, buf), 0);
                return TypedActionResult.consume(heldStack);
            }
        }
        return TypedActionResult.pass(heldStack);
    }

    @Override
    public Text getDisplayName() {
        return new LiteralText("");
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        ItemStack bookStack = player.getMainHandStack();
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
        boolean canLink = !player.getEntityWorld().getRegistryKey().getValue().equals(linkData.getDimension())
                || linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING);
        LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
        CompoundTag image = persistentState.getLinkingPanelImage(linkData.getUUID());
        return new LinkingBookScreenHandler(syncId, playerInventory, false, bookStack, canLink, image);
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buffer) {
        ItemStack heldStack = player.getMainHandStack();
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(heldStack);
        ColorComponent colorData = ModComponents.ITEM_COLOR.get(heldStack);
        buffer.writeBoolean(true);
        buffer.writeInt(colorData.getColor());
        linkData.writeToPacketByteBuf(buffer);
        boolean canLink = !player.getEntityWorld().getRegistryKey().getValue().equals(linkData.getDimension())
                || linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING);
        buffer.writeBoolean(canLink);
        LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
        CompoundTag image = persistentState.getLinkingPanelImage(linkData.getUUID());
        buffer.writeCompoundTag(image);
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext tooltipContext) {
        super.appendTooltip(stack, world, tooltip, tooltipContext);
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(stack);
        tooltip.add(new LiteralText("�eAge: �9�o" + linkData.getDimension().toString()));
        tooltip.add(new LiteralText("�ePosition: �9�o(" + linkData.getPosition().getX() + ", "
                + linkData.getPosition().getY() + ", " + linkData.getPosition().getZ() + ")"));
        Set<LinkEffect> linkEffects = new HashSet<LinkEffect>(linkData.getLinkEffects());
        linkEffects.remove(LinkEffects.DUMMY);
        if (!linkEffects.isEmpty()) {
            tooltip.add(new LiteralText("�eLink Effects:"));
            for (LinkEffect effect : linkEffects) {
                tooltip.add(new LiteralText("    �9�o"
                        + new TranslatableText("linkEffect." + ModRegistries.LINK_EFFECTS.getId(effect).getNamespace()
                                + "." + ModRegistries.LINK_EFFECTS.getId(effect).getPath()).getString()));
            }
        }
    }

}
