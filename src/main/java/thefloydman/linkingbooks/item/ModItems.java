/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.item;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.TallBlockItem;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.block.ModBlocks;
import thefloydman.linkingbooks.util.Reference.BlockNames;
import thefloydman.linkingbooks.util.Reference.ItemNames;

public class ModItems {

    public static final BlockItem LINKING_LECTERN = new BlockItem(ModBlocks.LINKING_LECTERN,
            new Item.Settings().group(ModItemGroups.LINKING_BOOKS_MAIN));

    public static final BlockItem MARKER_SWITCH = new TallBlockItem(ModBlocks.MARKER_SWITCH,
            new Item.Settings().group(ModItemGroups.LINKING_BOOKS_MAIN));

    public static final BlockItem NARA = new BlockItem(ModBlocks.NARA,
            new Item.Settings().group(ModItemGroups.LINKING_BOOKS_MAIN));

    public static final BlockItem LINK_TRANSLATOR = new BlockItem(ModBlocks.LINK_TRANSLATOR,
            new Item.Settings().group(ModItemGroups.LINKING_BOOKS_MAIN));

    public static final BlockItem BOOKSHELF_STAIRS = new BlockItem(ModBlocks.BOOKSHELF_STAIRS,
            new Item.Settings().group(ModItemGroups.LINKING_BOOKS_MAIN));

    public static final Item BLANK_LINKING_BOOK = new BlankLinkingBookItem(new FabricItemSettings().maxCount(16));

    public static final Item WRITTEN_LINKING_BOOK = new WrittenLinkingBookItem(new FabricItemSettings().maxCount(1));

    public static final Item LINKING_PANEL = new LinkingPanelItem(
            new FabricItemSettings().group(ModItemGroups.LINKING_BOOKS_MAIN));

    public static void register() {
        Registry.register(Registry.ITEM, BlockNames.LINKING_LECTERN, LINKING_LECTERN);
        Registry.register(Registry.ITEM, BlockNames.MARKER_SWITCH, MARKER_SWITCH);
        Registry.register(Registry.ITEM, BlockNames.NARA, NARA);
        Registry.register(Registry.ITEM, BlockNames.LINK_TRANSLATOR, LINK_TRANSLATOR);
        Registry.register(Registry.ITEM, BlockNames.BOOKSHELF_STAIRS, BOOKSHELF_STAIRS);
        Registry.register(Registry.ITEM, ItemNames.BLANK_LINKNG_BOOK, BLANK_LINKING_BOOK);
        Registry.register(Registry.ITEM, ItemNames.WRITTEN_LINKNG_BOOK, WRITTEN_LINKING_BOOK);
        Registry.register(Registry.ITEM, ItemNames.LINKING_PANEL, LINKING_PANEL);
    }

}
