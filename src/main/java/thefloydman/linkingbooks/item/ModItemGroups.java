/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.item;

import java.util.function.Predicate;

import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.util.ColorUtils;
import thefloydman.linkingbooks.util.Reference;

public class ModItemGroups {

    public static final ItemGroup LINKING_BOOKS_MAIN = FabricItemGroupBuilder
            .create(new Identifier(Reference.MOD_ID, "main"))
            .icon(() -> ModItems.WRITTEN_LINKING_BOOK.getDefaultStack()).appendItems(items -> {
                /*
                 * Since this method overwrites any items already in the group, we need to find
                 * and re-add them.
                 */
                Registry.ITEM.stream().filter(isInLinkingBooksGroup()).map(ItemStack::new).forEach(items::add);
                /*
                 * Adds a blank linking book to the Linking Books creative tab for each of the
                 * 16 standard dye colors.
                 */
                for (DyeColor dyeColor : DyeColor.values()) {
                    ItemStack stack = ModItems.BLANK_LINKING_BOOK.getDefaultStack();
                    ColorComponent color = ModComponents.ITEM_COLOR.get(stack);
                    color.setColor(ColorUtils.dyeColorAsInt(dyeColor));
                    items.add(stack);
                }
            }).build();

    private static Predicate<Item> isInLinkingBooksGroup() {
        return item -> item.getGroup() == LINKING_BOOKS_MAIN;
    }

}
