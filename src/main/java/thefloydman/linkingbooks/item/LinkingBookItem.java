/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import thefloydman.linkingbooks.component.ModComponents;

public abstract class LinkingBookItem extends Item {

    public LinkingBookItem(Settings settings) {
        super(settings);
    }

    /**
     * Used to color item texture. Any tintIndex besides 0 will return -1.
     */
    public static int getColor(ItemStack stack, int tintIndex) {
        if (tintIndex == 0) {
            return ModComponents.ITEM_COLOR.get(stack).getColor();
        }
        return -1;
    }

}
