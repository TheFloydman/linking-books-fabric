/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.event;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.block.LinkTranslatorBlock;
import thefloydman.linkingbooks.block.LinkingLecternBlock;
import thefloydman.linkingbooks.block.MarkerSwitchBlock;
import thefloydman.linkingbooks.block.entity.LinkTranslatorBlockEntity;
import thefloydman.linkingbooks.block.entity.LinkingBookHolderBlockEntity;
import thefloydman.linkingbooks.block.entity.MarkerSwitchBlockEntity;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.entity.LinkingBookEntity;
import thefloydman.linkingbooks.integration.ImmersivePortalsIntegration;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.util.LinkingPortalArea;
import thefloydman.linkingbooks.util.Reference;

public class ModEventHandlers {

    public static ActionResult playerTossItem(PlayerEntity player, ItemEntity itemEntity) {
        ItemStack stack = itemEntity.getStack();
        if (stack.getItem() instanceof WrittenLinkingBookItem) {
            World world = itemEntity.getEntityWorld();
            if (!world.isClient()) {
                LinkingBookEntity entity = new LinkingBookEntity(world, stack.copy());
                Vec3d lookVec = player.getRotationVector();
                entity.updatePositionAndAngles(player.getX(), player.getY() + 1.75D + lookVec.getY(),
                        player.getZ() + lookVec.getZ(), player.headYaw, 0.0F);
                entity.addVelocity(lookVec.x / 4, lookVec.y / 4, lookVec.z / 4);
                world.spawnEntity(entity);
            }
            return ActionResult.FAIL;
        }
        return ActionResult.PASS;
    }

    public static ActionResult playerUseBlock(PlayerEntity player, World world, Hand hand, BlockHitResult hitResult) {
        if (world.isClient() || hand.equals(Hand.OFF_HAND) || !player.isSneaking()) {
            return ActionResult.PASS;
        }
        BlockPos pos = hitResult.getBlockPos();
        if (world.getBlockState(pos).getBlock() instanceof LinkingLecternBlock
                || world.getBlockState(pos).getBlock() instanceof LinkTranslatorBlock) {
            BlockEntity generic = world.getBlockEntity(pos);
            if (!(generic instanceof LinkingBookHolderBlockEntity)) {
                return ActionResult.PASS;
            }
            LinkingBookHolderBlockEntity blockEntity = (LinkingBookHolderBlockEntity) generic;
            ItemStack stack = player.getStackInHand(hand);
            if (stack.getItem() instanceof WrittenLinkingBookItem && !blockEntity.hasBook()) {
                LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(stack);
                blockEntity.setBook(stack);
                player.playerScreenHandler.sendContentUpdates();
                if (world.getBlockState(pos).getBlock() instanceof LinkTranslatorBlock) {
                    LinkTranslatorBlockEntity linkTranslatorBlockEntity = (LinkTranslatorBlockEntity) blockEntity;
                    LinkingPortalArea.tryMakeLinkingPortalOnEveryAxis(world, pos, linkData, linkTranslatorBlockEntity);
                }
                return ActionResult.CONSUME;
            } else if (stack.isEmpty() && blockEntity.hasBook()) {
                player.giveItemStack(blockEntity.getBook());
                player.playerScreenHandler.sendContentUpdates();
                blockEntity.setBook(ItemStack.EMPTY);
                if (world.getBlockState(pos).getBlock() instanceof LinkTranslatorBlock) {
                    LinkTranslatorBlockEntity linkTranslatorBlockEntity = (LinkTranslatorBlockEntity) blockEntity;
                    if (Reference.isImmersivePortalsLoaded()) {
                        ImmersivePortalsIntegration.deleteLinkingPortals(linkTranslatorBlockEntity);
                    }
                    LinkingPortalArea.tryEraseLinkingPortalOnEveryAxis(world, pos);
                }
                return ActionResult.CONSUME;
            }
        } else if (world.getBlockState(pos).getBlock() instanceof MarkerSwitchBlock) {
            BlockState state = world.getBlockState(pos);
            if (state.get(MarkerSwitchBlock.OPEN) == true) {
                BlockEntity generic = world.getBlockEntity(pos);
                if (generic instanceof MarkerSwitchBlockEntity) {
                    MarkerSwitchBlockEntity blockEntity = (MarkerSwitchBlockEntity) generic;
                    MarkerSwitchBlockEntity twinEntity = (MarkerSwitchBlockEntity) (state
                            .get(MarkerSwitchBlock.HALF) == DoubleBlockHalf.LOWER ? world.getBlockEntity(pos.up())
                                    : world.getBlockEntity(pos.down()));
                    ItemStack stack = player.getStackInHand(hand);
                    if (blockEntity.getStack(0).isEmpty()) {
                        blockEntity.setStack(0, stack);
                        twinEntity.setStack(0, stack);
                        stack.setCount(0);
                        player.playerScreenHandler.sendContentUpdates();
                    } else if (!blockEntity.getStack(0).isEmpty()) {
                        player.giveItemStack(blockEntity.removeStack(0));
                        player.playerScreenHandler.sendContentUpdates();
                        twinEntity.removeStack(0);
                    }
                }
            }
        }
        return ActionResult.PASS;
    }

    public static ActionResult entityJoinWorld(Entity entity, World world) {
        if (entity instanceof ItemEntity) {
            ItemStack stack = ((ItemEntity) entity).getStack();
            Item item = stack.getItem();
            if (item instanceof WrittenLinkingBookItem) {
                Entity newEntity = new LinkingBookEntity(world, stack.copy());
                newEntity.updatePositionAndAngles(entity.getX(), entity.getY(), entity.getZ(), entity.yaw, 0.0F);
                newEntity.setVelocity(entity.getVelocity());
                entity.remove();
                world.spawnEntity(newEntity);
                return ActionResult.FAIL;
            }
        }
        return ActionResult.PASS;
    }

    public static void serverStarted(MinecraftServer server) {
        Reference.server = server;
    }

}
