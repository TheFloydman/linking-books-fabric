/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.Entity;
import net.minecraft.util.ActionResult;
import net.minecraft.world.World;

/**
 * Callback for when an entity joins a world. Called before the entity is
 * actually spawned. Upon return:<br/>
 * - SUCCESS cancels further processing and continues with normal spawning
 * behavior.<br/>
 * - PASS falls back to further processing and defaults to SUCCESS if no other
 * listeners are available<br/>
 * - FAIL cancels further processing and does not spawn the ItemEntity.
 **/
public interface EntityJoinWorldCallback {

    Event<EntityJoinWorldCallback> EVENT = EventFactory.createArrayBacked(EntityJoinWorldCallback.class,
            (listeners) -> (entity, world) -> {
                for (EntityJoinWorldCallback listener : listeners) {
                    ActionResult result = listener.interact(entity, world);

                    if (result != ActionResult.PASS) {
                        return result;
                    }
                }

                return ActionResult.PASS;
            });

    ActionResult interact(Entity entity, World world);

}
