/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.api.component;

import java.util.Set;
import java.util.UUID;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import thefloydman.linkingbooks.api.linking.LinkEffect;

public interface LinkDataComponent {

    public void setDimension(Identifier dimension);

    public Identifier getDimension();

    public void setPosition(BlockPos position);

    public BlockPos getPosition();

    public void setRotation(float rotation);

    public float getRotation();

    public void setUUID(UUID uuid);

    public UUID getUUID();

    public void setLinkEffects(Set<LinkEffect> effects);

    public Set<LinkEffect> getLinkEffects();

    public boolean addLinkEffect(LinkEffect effect);

    public boolean removeLinkEffect(LinkEffect effect);

    public PacketByteBuf writeToPacketByteBuf(PacketByteBuf buffer);

    public void readFromPacketByteBuf(PacketByteBuf buffer);

}
