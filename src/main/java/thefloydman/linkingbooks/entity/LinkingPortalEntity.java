/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.entity;

import com.qouteall.immersive_portals.portal.Portal;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.component.ItemLinkData;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.config.ModConfig;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.linking.LinkEffects;

/**
 * Do NOT reference this class without first checking that Immersive Portals is
 * installed.
 */
public class LinkingPortalEntity extends Portal {

    private static final TrackedData<ItemStack> ITEM = DataTracker.registerData(LinkingPortalEntity.class,
            TrackedDataHandlerRegistry.ITEM_STACK);
    private static final TrackedData<BlockPos> BLOCKENTITY_POS = DataTracker.registerData(LinkingPortalEntity.class,
            TrackedDataHandlerRegistry.BLOCK_POS);

    public LinkingPortalEntity(EntityType<?> entityType, World world, ItemStack book, BlockPos blockEntityPos) {
        super(entityType, world);
        this.dataTracker.set(ITEM, book == null ? ItemStack.EMPTY : book);
        this.dataTracker.set(BLOCKENTITY_POS, blockEntityPos == null ? BlockPos.ORIGIN : blockEntityPos);
    }

    public LinkingPortalEntity(EntityType<?> entityType, World world) {
        this(entityType, world, ItemStack.EMPTY, BlockPos.ORIGIN);
    }

    @Override
    protected void initDataTracker() {
        this.dataTracker.startTracking(ITEM, ItemStack.EMPTY);
        this.dataTracker.startTracking(BLOCKENTITY_POS, BlockPos.ORIGIN);
    }

    public BlockPos getBlockEntityPos() {
        return this.dataTracker.get(BLOCKENTITY_POS);
    }

    public void setBlockEntityPos(BlockPos pos) {
        this.dataTracker.set(BLOCKENTITY_POS, pos);
    }

    @Override
    public void onEntityTeleportedOnServer(Entity entity) {
        super.onEntityTeleportedOnServer(entity);
        if (!this.dataTracker.get(ITEM).isEmpty()) {
            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(this.dataTracker.get(ITEM));
            for (LinkEffect effect : linkData.getLinkEffects()) {
                effect.onLinkStart(entity, linkData);
                effect.onLinkEnd(entity, linkData);
            }
        }
        if (entity instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) entity;
            if (!player.isCreative()) {
                ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
                player.addExperienceLevels(config.linkingCostExperienceLevels * -1);
            }
        }
    }

    @Override
    public boolean canTeleportEntity(Entity entity) {
        boolean ip = super.canTeleportEntity(entity);
        boolean lb = true;
        if (!this.dataTracker.get(ITEM).isEmpty()) {
            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(this.dataTracker.get(ITEM));
            if ((this.getDestWorld() == this.getOriginWorld())
                    && !linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING)) {
                lb = false;
            } else if (entity instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) entity;
                ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
                lb = player.isCreative() || player.experienceLevel >= config.linkingCostExperienceLevels;
            }
            for (LinkEffect effect : linkData.getLinkEffects()) {
                if (!effect.canStartLink(entity, linkData) || !effect.canFinishLink(entity, linkData)) {
                    lb = false;
                    break;
                }
            }
        }
        return ip && lb;
    }

    @Override
    protected void readCustomDataFromTag(CompoundTag compound) {
        super.readCustomDataFromTag(compound);
        if (compound.contains("book", NbtType.COMPOUND)) {
            ItemStack book = ItemStack.fromTag(compound.getCompound("book"));
            if (book.getItem() instanceof WrittenLinkingBookItem) {
                this.dataTracker.set(ITEM, book);
            } else {
                this.dataTracker.set(ITEM, ItemStack.EMPTY);
            }
        }
        if (compound.contains("blockentity_pos", NbtType.COMPOUND)) {
            this.dataTracker.set(BLOCKENTITY_POS, NbtHelper.toBlockPos(compound.getCompound("blockentity_pos")));
        }
    }

    @Override
    protected void writeCustomDataToTag(CompoundTag compound) {
        super.writeCustomDataToTag(compound);
        ItemStack item = this.dataTracker.get(ITEM);
        if (!item.isEmpty()) {
            ItemLinkData data = ModComponents.ITEM_LINK_DATA.get(item);
            /* TODO: Figure out why this is even necessary to prevent save errors. */
            if (!data.getLinkEffects().contains(LinkEffects.DUMMY)) {
                data.addLinkEffect(LinkEffects.DUMMY);
            }
            compound.put("book", item.toTag(new CompoundTag()));
        }
        compound.put("blockentity_pos", NbtHelper.fromBlockPos(this.dataTracker.get(BLOCKENTITY_POS)));
    }

}
