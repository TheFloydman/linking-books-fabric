/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.entity;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MovementType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import thefloydman.linkingbooks.component.ItemLinkData;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.network.packets.s2c.SpawnEntityPacket;

public abstract class ObjectEntity extends Entity {

    private static final TrackedData<Float> DURABILITY = DataTracker.registerData(ObjectEntity.class,
            TrackedDataHandlerRegistry.FLOAT);
    private static final TrackedData<ItemStack> ITEM = DataTracker.registerData(ObjectEntity.class,
            TrackedDataHandlerRegistry.ITEM_STACK);

    private static final String LABEL_DURABILITY = "Durability";
    private static final String LABEL_ITEM = "Item";
    private static final String LABEL_HURTTIME = "HurtTime";

    private final Class<? extends Item> itemClass;
    private final float maxDurability;
    public int hurtTime;

    public ObjectEntity(EntityType<? extends ObjectEntity> type, World world, Class<? extends Item> itemClass,
            float maxDurability) {
        super(type, world);
        this.itemClass = itemClass;
        this.maxDurability = maxDurability;
        this.setDurability(this.getMaxDurability());
        this.hurtTime = 0;
    }

    public ObjectEntity(EntityType<? extends ObjectEntity> type, World world, Class<? extends Item> itemClass,
            float maxDurability, ItemStack item) {
        this(type, world, itemClass, maxDurability);
        this.setItem(item);
    }

    @Override
    public boolean isAttackable() {
        return this.isAlive();
    }

    @Override
    protected void initDataTracker() {
        this.dataTracker.startTracking(DURABILITY, 1.0F);
        this.dataTracker.startTracking(ITEM, ItemStack.EMPTY);
    }

    @Override
    protected void readCustomDataFromTag(CompoundTag compound) {
        if (compound.contains(LABEL_DURABILITY, NbtType.NUMBER)) {
            this.setDurability(compound.getFloat(LABEL_DURABILITY));
        }
        if (compound.contains(LABEL_ITEM, NbtType.COMPOUND)) {
            ItemStack stack = ItemStack.fromTag(compound.getCompound(LABEL_ITEM));
            if (this.itemClass.isInstance(stack.getItem())) {
                this.setItem(stack);
            } else {
                this.setItem(ItemStack.EMPTY);
            }
        }

        this.hurtTime = compound.getShort(LABEL_HURTTIME);
    }

    @Override
    protected void writeCustomDataToTag(CompoundTag compound) {
        compound.putFloat(LABEL_DURABILITY, this.getDurability());
        ItemStack item = this.getItem();
        ItemLinkData data = ModComponents.ITEM_LINK_DATA.get(item);
        /* TODO: Figure out why this is even necessary to prevent save errors. */
        if (!data.getLinkEffects().contains(LinkEffects.DUMMY)) {
            data.addLinkEffect(LinkEffects.DUMMY);
        }
        compound.put(LABEL_ITEM, item.toTag(new CompoundTag()));
        compound.putShort(LABEL_HURTTIME, (short) this.hurtTime);
    }

    /**
     * Makes sure the entity spawns correctly client-side.
     * 
     * @return
     */
    @Override
    public Packet<?> createSpawnPacket() {
        PacketByteBuf buf = new SpawnEntityPacket(this).toData(new PacketByteBuf(Unpooled.buffer()));
        return ServerPlayNetworking.createS2CPacket(SpawnEntityPacket.CHANNEL, buf);
    }

    public final float getMaxDurability() {
        return this.maxDurability;
    }

    public float getDurability() {
        return this.dataTracker.get(DURABILITY);
    }

    public void setDurability(float health) {
        this.dataTracker.set(DURABILITY, MathHelper.clamp(health, 0.0F, this.getMaxDurability()));
    }

    public ItemStack getItem() {
        return this.dataTracker.get(ITEM);
    }

    public void setItem(ItemStack item) {
        this.dataTracker.set(ITEM, item);
    }

    @Override
    public boolean damage(DamageSource source, float amount) {
        this.hurtTime = 10;
        this.setDurability(this.getDurability() - amount);
        return super.damage(source, amount);
    }

    @Override
    public void baseTick() {
        super.baseTick();
        if (this.getItem().isEmpty() || this.getDurability() <= 0.0F) {
            this.onKilled();
        }
        if (this.hurtTime > 0) {
            --this.hurtTime;
        }
        if (!this.hasNoGravity()) {
            this.setVelocity(this.getVelocity().add(0.0D, -0.04D, 0.0D));
        }
        Vec3d vec3d = this.getVelocity();
        double d1 = vec3d.x;
        double d3 = vec3d.y;
        double d5 = vec3d.z;
        if (this.onGround) {
            d1 *= 0.8D;
            d5 *= 0.8D;
        }
        if (Math.abs(vec3d.x) < 0.003D) {
            d1 = 0.0D;
        }
        if (Math.abs(vec3d.y) < 0.003D) {
            d3 = 0.0D;
        }
        if (Math.abs(vec3d.z) < 0.003D) {
            d5 = 0.0D;
        }
        this.setVelocity(d1, d3, d5);
        this.move(MovementType.SELF, this.getVelocity());
    }

    public void onKilled() {
        this.kill();
    }

    @Override
    public void onPlayerCollision(PlayerEntity player) {
        if (this.distanceTo(player) < 0.75F) {
            player.pushAwayFrom(this);
        }
    }

    @Override
    public boolean isCollidable() {
        return this.isAlive();
    }

    @Override
    public boolean collides() {
        return this.isAlive();
    }

    @Override
    public boolean isPushable() {
        return this.isAlive();
    }

    @Override
    @Environment(EnvType.CLIENT)
    public boolean shouldRender(double distance) {
        double d0 = this.getBoundingBox().getAverageSideLength();
        if (Double.isNaN(d0)) {
            d0 = 1.0D;
        }

        d0 = d0 * 256.0D * getRenderDistanceMultiplier();
        return distance < d0 * d0;
    }

}
