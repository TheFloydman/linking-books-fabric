/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.entity;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.fabricmc.fabric.impl.screenhandler.Networking;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.screen.LinkingBookScreenHandler;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.world.LinkingBooksPersistentState;

public class LinkingBookEntity extends ObjectEntity implements ExtendedScreenHandlerFactory {

    protected LinkingBookEntity(EntityType<? extends LinkingBookEntity> type, World world) {
        super(type, world, WrittenLinkingBookItem.class, 10.0F);
        if (world.isClient()) {
            setRenderDistanceMultiplier(2.0D);
        }
    }

    protected LinkingBookEntity(EntityType<? extends LinkingBookEntity> type, World world, ItemStack item) {
        super(type, world, WrittenLinkingBookItem.class, 10.0F, item);
        if (world.isClient()) {
            setRenderDistanceMultiplier(2.0D);
        }
    }

    public LinkingBookEntity(World world) {
        this(ModEntityTypes.LINKING_BOOK, world);
    }

    public LinkingBookEntity(World world, ItemStack item) {
        this(ModEntityTypes.LINKING_BOOK, world, item);
    }

    @Override
    public ActionResult interactAt(PlayerEntity player, Vec3d hitPos, Hand hand) {
        if (!player.getEntityWorld().isClient()) {
            ServerPlayerEntity serverPlayer = (ServerPlayerEntity) player;
            if (hand == Hand.MAIN_HAND) {
                ItemStack bookStack = this.getItem();
                if (!bookStack.isEmpty()) {
                    if (serverPlayer.isSneaking()) {
                        serverPlayer.giveItemStack(bookStack);
                        serverPlayer.playerScreenHandler.sendContentUpdates();
                        this.remove();
                        return ActionResult.SUCCESS;
                    } else {
                        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
                        ColorComponent color = ModComponents.ITEM_COLOR.get(bookStack);
                        if (linkData != null && color != null) {
                            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
                            this.writeScreenOpeningData((ServerPlayerEntity) player, buf);
                            Networking.sendOpenPacket((ServerPlayerEntity) player, this,
                                    new LinkingBookScreenHandler(0, player.inventory, buf), 0);
                            return ActionResult.CONSUME;
                        }
                    }
                }
            }
        }
        return ActionResult.PASS;
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        ItemStack bookStack = this.getItem();
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
        boolean canLink = !player.getEntityWorld().getRegistryKey().getValue().equals(linkData.getDimension())
                || linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING);
        LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
        CompoundTag image = persistentState.getLinkingPanelImage(linkData.getUUID());
        return new LinkingBookScreenHandler(syncId, playerInventory, false, bookStack, canLink, image);
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
        ItemStack bookStack = this.getItem();
        ColorComponent colorData = ModComponents.ITEM_COLOR.get(bookStack);
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
        boolean canLink = !player.getEntityWorld().getRegistryKey().getValue().equals(linkData.getDimension())
                || linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING);
        LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
        CompoundTag image = persistentState.getLinkingPanelImage(linkData.getUUID());
        buf.writeBoolean(false);
        buf.writeInt(colorData.getColor());
        linkData.writeToPacketByteBuf(buf);
        buf.writeBoolean(canLink);
        buf.writeCompoundTag(image);
    }

}
