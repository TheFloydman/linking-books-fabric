/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.entity;

import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.integration.ImmersivePortalsIntegration;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.util.Reference.EntityNames;

public class ModEntityTypes {

    public static final EntityType<LinkingBookEntity> LINKING_BOOK = FabricEntityTypeBuilder
            .<LinkingBookEntity>create(SpawnGroup.MISC, LinkingBookEntity::new)
            .dimensions(EntityDimensions.fixed(0.3F, 0.1F)).trackRangeChunks(16).build();

    public static void register() {
        Registry.register(Registry.ENTITY_TYPE, EntityNames.LINKING_BOOK, LINKING_BOOK);
        if (Reference.isImmersivePortalsLoaded()) {
            ImmersivePortalsIntegration.registerImmersivePortalsEntities();
        }
    }

}
