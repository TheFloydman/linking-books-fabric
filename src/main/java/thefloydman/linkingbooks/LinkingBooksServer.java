/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks;

import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import thefloydman.linkingbooks.network.packets.c2s.LinkPacket;
import thefloydman.linkingbooks.network.packets.c2s.SaveLinkingPanelImagePacket;
import thefloydman.linkingbooks.network.packets.c2s.ServerPacketHandlers;

public class LinkingBooksServer implements DedicatedServerModInitializer {

    @Override
    public void onInitializeServer() {
        // Register packet handlers.
        ServerPlayNetworking.registerGlobalReceiver(LinkPacket.CHANNEL, ServerPacketHandlers::handleLinkPacketonServer);
        ServerPlayNetworking.registerGlobalReceiver(SaveLinkingPanelImagePacket.CHANNEL,
                ServerPacketHandlers::handleSaveImagePacketonServer);
    }

}
