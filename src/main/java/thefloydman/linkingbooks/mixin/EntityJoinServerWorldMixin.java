/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.Entity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import thefloydman.linkingbooks.event.EntityJoinWorldCallback;

@Mixin(ServerWorld.class)
public class EntityJoinServerWorldMixin {

    @Inject(at = @At("HEAD"), method = "addPlayer(Lnet/minecraft/server/network/ServerPlayerEntity;)V", cancellable = true)
    private void addPlayer(ServerPlayerEntity player, CallbackInfo info) {
        ActionResult result = EntityJoinWorldCallback.EVENT.invoker().interact(player, (ServerWorld) (Object) this);

        if (result == ActionResult.FAIL) {
            info.cancel();
        }

        return;
    }

    @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/server/world/ServerWorld;getChunk(IILnet/minecraft/world/chunk/ChunkStatus;Z)Lnet/minecraft/world/chunk/Chunk;"), method = "addEntity(Lnet/minecraft/entity/Entity;)Z")
    private void addEntity(Entity entity, CallbackInfoReturnable<Boolean> info) {
        ActionResult result = EntityJoinWorldCallback.EVENT.invoker().interact(entity, (ServerWorld) (Object) this);

        if (result == ActionResult.CONSUME) {
            info.setReturnValue(false);
        }
    }

    @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/server/world/ServerWorld;loadEntityUnchecked(Lnet/minecraft/entity/Entity;)V"), method = "loadEntity(Lnet/minecraft/entity/Entity;)Z")
    public void loadEntity(Entity entity, CallbackInfoReturnable<Boolean> info) {
        ActionResult result = EntityJoinWorldCallback.EVENT.invoker().interact(entity, (ServerWorld) (Object) this);

        if (result == ActionResult.CONSUME) {
            info.setReturnValue(false);
        }
    }

}
