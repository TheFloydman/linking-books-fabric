/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.util.ActionResult;
import thefloydman.linkingbooks.event.EntityJoinWorldCallback;

@Mixin(ClientWorld.class)
public class EntityJoinClientWorldMixin {

    @Inject(at = @At("HEAD"), method = "addEntityPrivate(ILnet/minecraft/entity/Entity;)V", cancellable = true)
    private void addEntityPrivate(int id, Entity entity, final CallbackInfo info) {
        ActionResult result = EntityJoinWorldCallback.EVENT.invoker().interact(entity, (ClientWorld) (Object) this);

        if (result == ActionResult.FAIL) {
            info.cancel();
        }

        return;
    }

}
