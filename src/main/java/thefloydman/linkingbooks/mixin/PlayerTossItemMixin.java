/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import thefloydman.linkingbooks.event.PlayerTossItemCallback;

@Mixin(PlayerEntity.class)
public class PlayerTossItemMixin {

    @Inject(at = @At("TAIL"), method = "dropItem(Lnet/minecraft/item/ItemStack;ZZ)Lnet/minecraft/entity/ItemEntity;", cancellable = true)
    public ItemEntity dropItem(ItemStack stack, boolean throwRandomly, boolean retainOwnership,
            final CallbackInfoReturnable<ItemEntity> info) {
        ActionResult result = PlayerTossItemCallback.EVENT.invoker().interact((PlayerEntity) (Object) this,
                info.getReturnValue());

        if (result == ActionResult.FAIL) {
            return new ItemEntity(info.getReturnValue().world, info.getReturnValue().getX(),
                    info.getReturnValue().getY(), info.getReturnValue().getZ(), ItemStack.EMPTY);
        }

        return info.getReturnValue();
    }

}
