/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.mixin.StairsBlockMixin;
import thefloydman.linkingbooks.util.Reference.BlockNames;

public class ModBlocks {

    public static final Block LINKING_LECTERN = new LinkingLecternBlock(
            FabricBlockSettings.of(Material.WOOD).strength(5.0f).breakByTool(FabricToolTags.AXES, 2).nonOpaque());

    public static final Block MARKER_SWITCH = new MarkerSwitchBlock(
            FabricBlockSettings.of(Material.WOOD).strength(5.0f).breakByTool(FabricToolTags.AXES, 2).nonOpaque());

    public static final Block NARA = new NaraBlock(
            AbstractBlock.Settings.of(Material.METAL, MaterialColor.BROWN).requiresTool().strength(100.0F, 2400.0F));

    public static final Block LINK_TRANSLATOR = new LinkTranslatorBlock(
            AbstractBlock.Settings.of(Material.METAL, MaterialColor.BROWN).requiresTool().strength(25.0F, 600.0F));

    public static final Block LINKING_PORTAL = new LinkingPortalBlock(AbstractBlock.Settings.of(Material.PORTAL)
            .noCollision().strength(-1.0F, -1.0F).sounds(BlockSoundGroup.GLASS).nonOpaque().luminance((state) -> {
                return 11;
            }));

    public static final Block BOOKSHELF_STAIRS = StairsBlockMixin.newStairsBlock(Blocks.SPRUCE_PLANKS.getDefaultState(),
            AbstractBlock.Settings.copy(Blocks.SPRUCE_PLANKS));

    public static void register() {
        Registry.register(Registry.BLOCK, BlockNames.LINKING_LECTERN, LINKING_LECTERN);
        Registry.register(Registry.BLOCK, BlockNames.MARKER_SWITCH, MARKER_SWITCH);
        Registry.register(Registry.BLOCK, BlockNames.NARA, NARA);
        Registry.register(Registry.BLOCK, BlockNames.LINK_TRANSLATOR, LINK_TRANSLATOR);
        Registry.register(Registry.BLOCK, BlockNames.LINKING_PORTAL, LINKING_PORTAL);
        Registry.register(Registry.BLOCK, BlockNames.BOOKSHELF_STAIRS, BOOKSHELF_STAIRS);
    }

}
