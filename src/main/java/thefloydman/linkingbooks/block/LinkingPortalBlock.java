/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ShapeContext;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.function.BooleanBiFunction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Direction.Axis;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import thefloydman.linkingbooks.util.LinkingUtils;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.world.LinkingBooksPersistentState;

/**
 * Much of this code is copied from
 * {@link net.minecraft.block.NetherPortalBlock}.
 *
 */
public class LinkingPortalBlock extends Block {

    public static final EnumProperty<Axis> AXIS = Properties.AXIS;
    protected static final VoxelShape X_SHAPE = Block.createCuboidShape(0.0D, 0.0D, 6.0D, 16.0D, 16.0D, 10.0D);
    protected static final VoxelShape Y_SHAPE = Block.createCuboidShape(0.0D, 6.0D, 0.0D, 16.0D, 10.0D, 16.0D);
    protected static final VoxelShape Z_SHAPE = Block.createCuboidShape(6.0D, 0.0D, 0.0D, 10.0D, 16.0D, 16.0D);

    public LinkingPortalBlock(Settings settings) {
        super(settings);
        this.setDefaultState(this.stateManager.getDefaultState().with(AXIS, Direction.Axis.X));
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(AXIS);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public ItemStack getPickStack(BlockView blockView, BlockPos blockPos, BlockState blockState) {
        return ItemStack.EMPTY;
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState blockState, Direction direction, BlockState blockState2,
            WorldAccess worldAccess, BlockPos blockPos, BlockPos blockPos2) {
        return !blockState2.isOf(this) ? Blocks.AIR.getDefaultState()
                : super.getStateForNeighborUpdate(blockState, direction, blockState2, worldAccess, blockPos, blockPos2);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState blockState, BlockView blockView, BlockPos blockPos,
            ShapeContext shapeContext) {
        switch (blockState.get(AXIS)) {
        case Z:
            return Z_SHAPE;
        case Y:
            return Y_SHAPE;
        case X:
        default:
            return X_SHAPE;
        }
    }

    /**
     * Copied from {@link net.minecraft.block.EndPortalBlock}.
     *
     */
    @Override
    public void onEntityCollision(BlockState blockState, World world, BlockPos blockPos, Entity entity) {
        if (!world.isClient() && !entity.hasVehicle() && !entity.hasPassengers() && entity.canUsePortals()
                && VoxelShapes.matchesAnywhere(
                        VoxelShapes.cuboid(entity.getBoundingBox().offset((-blockPos.getX()), (-blockPos.getY()),
                                (-blockPos.getZ()))),
                        blockState.getOutlineShape(world, blockPos), BooleanBiFunction.AND)) {
            LinkingBooksPersistentState persistentState = ((ServerWorld) world).getPersistentStateManager()
                    .getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);

            LinkingUtils.linkEntity(entity, persistentState.getLinkingPortalData(blockPos), false);
        }

    }

    @Override
    public void onStateReplaced(BlockState blockState, World world, BlockPos pos, BlockState blockState2, boolean bl) {
        if (blockState.getBlock() != blockState2.getBlock() && !world.isClient()) {
            LinkingBooksPersistentState persistentState = ((ServerWorld) world).getPersistentStateManager()
                    .getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
            persistentState.removeLinkingPortalData(pos);
        }
        super.onStateReplaced(blockState, world, pos, blockState2, bl);
    }

}
