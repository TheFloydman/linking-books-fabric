/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.impl.screenhandler.Networking;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalFacingBlock;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.pathing.NavigationType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.block.entity.LinkTranslatorBlockEntity;
import thefloydman.linkingbooks.block.entity.ModBlockEntityTypes;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.entity.LinkingBookEntity;
import thefloydman.linkingbooks.integration.ImmersivePortalsIntegration;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.screen.LinkingBookScreenHandler;
import thefloydman.linkingbooks.util.LinkingPortalArea;
import thefloydman.linkingbooks.util.Reference;

public class LinkTranslatorBlock extends HorizontalFacingBlock implements BlockEntityProvider {

    public static final BooleanProperty HAS_BOOK = Properties.HAS_BOOK;
    public static final VoxelShape NORTH_SHAPE = VoxelShapes.union(Block.createCuboidShape(0, 0, 2, 16, 16, 16),
            Block.createCuboidShape(0, 0, 0, 16, 5, 1), Block.createCuboidShape(0, 0, 1, 16, 4, 2),
            Block.createCuboidShape(0, 11, 0, 16, 16, 1), Block.createCuboidShape(0, 12, 1, 16, 16, 2));
    public static final VoxelShape EAST_SHAPE = VoxelShapes.union(Block.createCuboidShape(0, 0, 0, 14, 16, 16),
            Block.createCuboidShape(15, 0, 0, 16, 5, 16), Block.createCuboidShape(14, 0, 0, 15, 4, 16),
            Block.createCuboidShape(15, 11, 0, 16, 16, 16), Block.createCuboidShape(14, 12, 0, 15, 16, 16));
    public static final VoxelShape SOUTH_SHAPE = VoxelShapes.union(Block.createCuboidShape(0, 0, 0, 16, 16, 14),
            Block.createCuboidShape(0, 0, 15, 16, 5, 16), Block.createCuboidShape(0, 0, 14, 16, 4, 15),
            Block.createCuboidShape(0, 11, 15, 16, 16, 16), Block.createCuboidShape(0, 12, 14, 16, 16, 15));
    public static final VoxelShape WEST_SHAPE = VoxelShapes.union(Block.createCuboidShape(2, 0, 0, 16, 16, 16),
            Block.createCuboidShape(0, 0, 0, 1, 5, 16), Block.createCuboidShape(1, 0, 0, 2, 4, 16),
            Block.createCuboidShape(0, 11, 0, 1, 16, 16), Block.createCuboidShape(1, 12, 0, 2, 16, 16));

    protected LinkTranslatorBlock(Settings settings) {
        super(settings);
        this.setDefaultState(this.stateManager.getDefaultState().with(FACING, Direction.NORTH).with(HAS_BOOK, false));
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(FACING, HAS_BOOK);
    }

    @Override
    public boolean hasSidedTransparency(BlockState state) {
        return true;
    }

    @Override
    public boolean canPathfindThrough(BlockState state, BlockView world, BlockPos pos, NavigationType type) {
        return false;
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
            BlockHitResult hit) {
        BlockEntity generic = world.getBlockEntity(pos);
        if (generic instanceof LinkTranslatorBlockEntity) {
            LinkTranslatorBlockEntity blockEntity = (LinkTranslatorBlockEntity) generic;
            if (!world.isClient() && hand.equals(Hand.MAIN_HAND)) {
                if (player.isSneaking()) {
                    if (world.getBlockState(pos).getBlock() instanceof LinkingLecternBlock) {
                        ItemStack stack = player.getStackInHand(hand);
                        if (stack.getItem() instanceof WrittenLinkingBookItem && !blockEntity.hasBook()) {
                            blockEntity.setBook(stack);
                            player.playerScreenHandler.sendContentUpdates();
                        } else if (stack.isEmpty() && blockEntity.hasBook()) {
                            player.giveItemStack(blockEntity.getBook());
                            player.playerScreenHandler.sendContentUpdates();
                            blockEntity.setBook(ItemStack.EMPTY);
                        }
                    }
                } else if (blockEntity.hasBook()) {
                    ItemStack stack = blockEntity.getBook();
                    Item item = stack.getItem();
                    if (item instanceof WrittenLinkingBookItem) {
                        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(stack);
                        ColorComponent color = ModComponents.ITEM_COLOR.get(stack);
                        if (linkData != null && color != null) {
                            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
                            blockEntity.writeScreenOpeningData((ServerPlayerEntity) player, buf);
                            Networking.sendOpenPacket((ServerPlayerEntity) player, blockEntity,
                                    new LinkingBookScreenHandler(0, player.inventory, buf), 0);
                        }
                    }
                }
            }
        }

        return ActionResult.PASS;
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return this.getDefaultState().with(FACING, ctx.getPlayerFacing().getOpposite());
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return ModBlockEntityTypes.LINK_TRANSLATOR.instantiate();
    }

    @Override
    public void onPlaced(World world, BlockPos blockPos, BlockState blockState, LivingEntity livingEntity,
            ItemStack itemStack) {
        super.onPlaced(world, blockPos, blockState, livingEntity, itemStack);
        for (int x = blockPos.getX() - 32; x < blockPos.getX() + 32; x++) {
            for (int y = blockPos.getY() - 32; y < blockPos.getY() + 32; y++) {
                for (int z = blockPos.getZ() - 32; z < blockPos.getZ() + 32; z++) {
                    BlockPos currentPos = new BlockPos(x, y, z);
                    BlockEntity blockEntity = world.getBlockEntity(currentPos);
                    if (blockEntity != null && blockEntity instanceof LinkTranslatorBlockEntity) {
                        LinkTranslatorBlockEntity translator = (LinkTranslatorBlockEntity) blockEntity;
                        if (translator.hasBook()) {
                            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(translator.getBook());
                            LinkingPortalArea.tryMakeLinkingPortalOnEveryAxis(world, currentPos, linkData, translator);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock() && !world.isClient()) {
            BlockEntity tileEntity = world.getBlockEntity(pos);
            if (tileEntity instanceof LinkTranslatorBlockEntity) {
                LinkTranslatorBlockEntity translatorTE = (LinkTranslatorBlockEntity) tileEntity;
                if (Reference.isImmersivePortalsLoaded()) {
                    ImmersivePortalsIntegration.deleteLinkingPortals(translatorTE);
                }
                if (translatorTE.hasBook()) {
                    ItemStack stack = translatorTE.getBook();
                    if (stack.getItem() instanceof WrittenLinkingBookItem) {
                        LinkingBookEntity entity = new LinkingBookEntity(world, stack.copy());
                        entity.updatePositionAndAngles(pos.getX() + 0.5D, pos.getY() + 1.0D, pos.getZ() + 0.5D,
                                state.get(FACING).asRotation() + 180.0F, 0.0F);
                        world.spawnEntity(entity);
                    }
                }
            }
            for (int x = pos.getX() - 32; x < pos.getX() + 32; x++) {
                for (int y = pos.getY() - 32; y < pos.getY() + 32; y++) {
                    for (int z = pos.getZ() - 32; z < pos.getZ() + 32; z++) {
                        BlockPos currentPos = new BlockPos(x, y, z);
                        BlockEntity blockEntity = world.getBlockEntity(currentPos);
                        if (blockEntity != null && blockEntity instanceof LinkTranslatorBlockEntity) {
                            LinkTranslatorBlockEntity translator = (LinkTranslatorBlockEntity) blockEntity;
                            if (Reference.isImmersivePortalsLoaded()) {
                                ImmersivePortalsIntegration.deleteLinkingPortals(translator);
                            }
                            if (translator.hasBook()) {
                                LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(translator.getBook());
                                LinkingPortalArea.tryMakeLinkingPortalOnEveryAxis(world, currentPos, linkData,
                                        translator);
                            }
                        }
                    }
                }
            }
        }
        super.onStateReplaced(state, world, pos, newState, isMoving);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState blockState, BlockView blockView, BlockPos blockPos,
            ShapeContext shapeContext) {
        switch (blockState.get(FACING)) {
        case EAST:
            return EAST_SHAPE;
        case SOUTH:
            return SOUTH_SHAPE;
        case WEST:
            return WEST_SHAPE;
        default:
            return NORTH_SHAPE;
        }
    }

}
