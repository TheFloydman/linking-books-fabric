/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block.entity;

import java.util.stream.Collectors;

import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.nbt.StringTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.component.ItemColor;
import thefloydman.linkingbooks.component.ItemLinkData;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.screen.LinkingBookScreenHandler;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.util.registry.ModRegistries;
import thefloydman.linkingbooks.world.LinkingBooksPersistentState;

public abstract class LinkingBookHolderBlockEntity extends BlockEntity
        implements BlockEntityClientSerializable, ExtendedScreenHandlerFactory {

    public LinkingBookHolderBlockEntity(BlockEntityType<?> blockEntityType) {
        super(blockEntityType);
    }

    protected ItemStack book = ItemStack.EMPTY;

    /**
     * Returns the book in this display without removing it.
     */
    public ItemStack getBook() {
        return this.book;
    }

    public boolean setBook(ItemStack stack) {
        this.book = stack.split(1);
        this.markDirty();
        return true;
    }

    public boolean hasBook() {
        return this.getBook().getItem() instanceof WrittenLinkingBookItem;
    }

    @Override
    public void markDirty() {
        super.markDirty();
        if (!this.getWorld().isClient()) {
            this.sync();
        }
    }

    @Override
    public void fromTag(BlockState state, CompoundTag nbt) {
        super.fromTag(state, nbt);
        if (nbt.contains("color", NbtType.INT) && nbt.contains("dimension", NbtType.STRING)
                && nbt.contains("position", NbtType.COMPOUND) && nbt.contains("rotation", NbtType.FLOAT)
                && nbt.contains("link_effects", NbtType.LIST) && nbt.contains("uuid", NbtType.INT_ARRAY)) {
            ItemStack stack = ModItems.WRITTEN_LINKING_BOOK.getDefaultStack();
            ItemColor color = ModComponents.ITEM_COLOR.get(stack);
            color.setColor(nbt.getInt("color"));
            ItemLinkData data = ModComponents.ITEM_LINK_DATA.get(stack);
            data.setDimension(new Identifier(nbt.getString("dimension")));
            data.setPosition(NbtHelper.toBlockPos(nbt.getCompound("position")));
            data.setRotation(nbt.getFloat("rotation"));
            data.setLinkEffects(nbt.getList("link_effects", NbtType.STRING).stream().map((stringTag) -> {
                return stringTag.asString();
            }).map(Identifier::new).map(LinkEffect::get).collect(Collectors.toSet()));
            data.setUUID(nbt.getUuid("uuid"));
            this.book = stack;
        } else {
            this.book = ItemStack.EMPTY;
        }
    }

    @Override
    public CompoundTag toTag(CompoundTag nbt) {
        super.toTag(nbt);
        if (!(this.book.getItem() instanceof WrittenLinkingBookItem)) {
            return nbt;
        }
        ItemColor color = ModComponents.ITEM_COLOR.get(this.book);
        nbt.putInt("color", color.getColor());
        ItemLinkData data = ModComponents.ITEM_LINK_DATA.get(this.book);
        nbt.putString("dimension", data.getDimension().toString());
        nbt.put("position", NbtHelper.fromBlockPos(data.getPosition()));
        nbt.putFloat("rotation", data.getRotation());
        ListTag linkEffects = new ListTag();
        linkEffects.addAll(data.getLinkEffects().stream().map(ModRegistries.LINK_EFFECTS::getId)
                .map(Identifier::toString).map(StringTag::of).collect(Collectors.toList()));
        nbt.put("link_effects", linkEffects);
        nbt.putUuid("uuid", data.getUUID());
        return nbt;
    }

    @Override
    public void fromClientTag(CompoundTag tag) {
        // Air block is just a filler here. In our case it is not necessary for it to be
        // accurate.
        this.fromTag(Blocks.AIR.getDefaultState(), tag);
    }

    @Override
    public CompoundTag toClientTag(CompoundTag tag) {
        return this.toTag(tag);
    }

    @Override
    public Text getDisplayName() {
        return new LiteralText("");
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        ItemStack bookStack = this.getBook();
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
        boolean canLink = !player.getEntityWorld().getRegistryKey().getValue().equals(linkData.getDimension())
                || linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING);
        LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
        CompoundTag image = persistentState.getLinkingPanelImage(linkData.getUUID());
        return new LinkingBookScreenHandler(syncId, playerInventory, false, bookStack, canLink, image);
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
        ItemStack bookStack = this.getBook();
        ColorComponent colorData = ModComponents.ITEM_COLOR.get(bookStack);
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(bookStack);
        boolean canLink = !player.getEntityWorld().getRegistryKey().getValue().equals(linkData.getDimension())
                || linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING);
        LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
        CompoundTag image = persistentState.getLinkingPanelImage(linkData.getUUID());
        buf.writeBoolean(false);
        buf.writeInt(colorData.getColor());
        linkData.writeToPacketByteBuf(buf);
        buf.writeBoolean(canLink);
        buf.writeCompoundTag(image);
    }

}
