/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block.entity;

import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.block.ModBlocks;
import thefloydman.linkingbooks.util.Reference;

public class ModBlockEntityTypes {

    public static final BlockEntityType<LinkingLecternBlockEntity> LINKING_LECTERN = Registry.register(
            Registry.BLOCK_ENTITY_TYPE, Reference.BlockEntityNames.LINKING_LECTERN,
            BlockEntityType.Builder.create(LinkingLecternBlockEntity::new, ModBlocks.LINKING_LECTERN).build(null));

    public static final BlockEntityType<LinkTranslatorBlockEntity> LINK_TRANSLATOR = Registry.register(
            Registry.BLOCK_ENTITY_TYPE, Reference.BlockEntityNames.LINK_TRANSLATOR,
            BlockEntityType.Builder.create(LinkTranslatorBlockEntity::new, ModBlocks.LINK_TRANSLATOR).build(null));

    public static final BlockEntityType<MarkerSwitchBlockEntity> MARKER_SWITCH = Registry.register(
            Registry.BLOCK_ENTITY_TYPE, Reference.BlockEntityNames.MARKER_SWITCH,
            BlockEntityType.Builder.create(MarkerSwitchBlockEntity::new, ModBlocks.MARKER_SWITCH).build(null));

    public static void register() {
    }

}
