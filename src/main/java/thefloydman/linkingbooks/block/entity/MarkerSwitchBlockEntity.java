/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block.entity;

import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.collection.DefaultedList;

public class MarkerSwitchBlockEntity extends BlockEntity
        implements BlockEntityClientSerializable, ImplementedInventory {

    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(1, ItemStack.EMPTY);

    public MarkerSwitchBlockEntity() {
        super(ModBlockEntityTypes.MARKER_SWITCH);
    }

    @Override
    public void markDirty() {
        super.markDirty();
        this.sync();
    }

    @Override
    public void fromTag(BlockState state, CompoundTag nbt) {
        super.fromTag(state, nbt);
        Inventories.fromTag(nbt, this.items);
    }

    @Override
    public CompoundTag toTag(CompoundTag nbt) {
        Inventories.toTag(nbt, this.items);
        return super.toTag(nbt);
    }

    @Override
    public void fromClientTag(CompoundTag tag) {
        // Air block is just a filler here. In our case it is not necessary for it to be
        // accurate.
        this.fromTag(Blocks.AIR.getDefaultState(), tag);
        if (!tag.contains("Items", NbtType.LIST) || tag.getList("Items", NbtType.COMPOUND).isEmpty()) {
            this.items.set(0, ItemStack.EMPTY);
        }
    }

    @Override
    public CompoundTag toClientTag(CompoundTag tag) {
        return this.toTag(tag);
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return this.items;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack stack = ImplementedInventory.super.removeStack(slot);
        this.markDirty();
        return stack;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        ImplementedInventory.super.setStack(slot, stack.copy());
        this.markDirty();
    }

}
