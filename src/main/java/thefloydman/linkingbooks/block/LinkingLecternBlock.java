/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.block;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.impl.screenhandler.Networking;
import net.minecraft.block.BlockState;
import net.minecraft.block.LecternBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.block.entity.LinkingLecternBlockEntity;
import thefloydman.linkingbooks.block.entity.ModBlockEntityTypes;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.entity.LinkingBookEntity;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.screen.LinkingBookScreenHandler;

public class LinkingLecternBlock extends LecternBlock {

    public LinkingLecternBlock(Settings properties) {
        super(properties);
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return ModBlockEntityTypes.LINKING_LECTERN.instantiate();
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
            BlockHitResult hit) {
        BlockEntity generic = world.getBlockEntity(pos);
        if (generic instanceof LinkingLecternBlockEntity) {
            LinkingLecternBlockEntity blockEntity = (LinkingLecternBlockEntity) generic;
            if (!world.isClient() && hand.equals(Hand.MAIN_HAND)) {
                if (player.isSneaking()) {
                    if (world.getBlockState(pos).getBlock() instanceof LinkingLecternBlock) {
                        ItemStack stack = player.getStackInHand(hand);
                        if (stack.getItem() instanceof WrittenLinkingBookItem && !blockEntity.hasBook()) {
                            blockEntity.setBook(stack);
                            player.playerScreenHandler.sendContentUpdates();
                        } else if (stack.isEmpty() && blockEntity.hasBook()) {
                            player.giveItemStack(blockEntity.getBook());
                            player.playerScreenHandler.sendContentUpdates();
                            blockEntity.setBook(ItemStack.EMPTY);
                        }
                    }
                } else if (blockEntity.hasBook()) {
                    ItemStack stack = blockEntity.getBook();
                    Item item = stack.getItem();
                    if (item instanceof WrittenLinkingBookItem) {
                        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(stack);
                        ColorComponent color = ModComponents.ITEM_COLOR.get(stack);
                        if (linkData != null && color != null) {
                            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
                            blockEntity.writeScreenOpeningData((ServerPlayerEntity) player, buf);
                            Networking.sendOpenPacket((ServerPlayerEntity) player, blockEntity,
                                    new LinkingBookScreenHandler(0, player.inventory, buf), 0);
                        }
                    }
                }
            }
        }

        return ActionResult.PASS;
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock() && !world.isClient()) {
            BlockEntity tileEntity = world.getBlockEntity(pos);
            if (tileEntity instanceof LinkingLecternBlockEntity) {
                LinkingLecternBlockEntity lecternTE = (LinkingLecternBlockEntity) tileEntity;
                if (lecternTE.hasBook()) {
                    ItemStack stack = lecternTE.getBook();
                    if (stack.getItem() instanceof WrittenLinkingBookItem) {
                        LinkingBookEntity entity = new LinkingBookEntity(world, stack.copy());
                        entity.updatePositionAndAngles(pos.getX() + 0.5D, pos.getY() + 1.0D, pos.getZ() + 0.5D,
                                state.get(FACING).asRotation() + 180.0F, 0.0F);
                        world.spawnEntity(entity);
                    }
                }
                super.onStateReplaced(state, world, pos, newState, isMoving);
            }
        }
    }

}
