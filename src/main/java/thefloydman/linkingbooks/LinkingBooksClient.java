/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.minecraft.client.render.RenderLayer;
import thefloydman.linkingbooks.block.ModBlocks;
import thefloydman.linkingbooks.block.entity.ModBlockEntityTypes;
import thefloydman.linkingbooks.client.gui.screen.LinkingBookScreen;
import thefloydman.linkingbooks.client.renderer.blockentity.LinkTranslatorRenderer;
import thefloydman.linkingbooks.client.renderer.blockentity.LinkingLecternRenderer;
import thefloydman.linkingbooks.client.renderer.blockentity.MarkerSwitchRenderer;
import thefloydman.linkingbooks.client.renderer.entity.LinkingBookRenderer;
import thefloydman.linkingbooks.entity.ModEntityTypes;
import thefloydman.linkingbooks.integration.ImmersivePortalsIntegration;
import thefloydman.linkingbooks.item.LinkingBookItem;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.network.packets.s2c.ClientPacketHandlers;
import thefloydman.linkingbooks.network.packets.s2c.SpawnEntityPacket;
import thefloydman.linkingbooks.network.packets.s2c.TakeScreenshotForLinkingBookPacket;
import thefloydman.linkingbooks.screen.ModScreenHandlerTypes;
import thefloydman.linkingbooks.util.Reference;

public class LinkingBooksClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        // Register packet handlers.
        ClientPlayNetworking.registerGlobalReceiver(TakeScreenshotForLinkingBookPacket.CHANNEL,
                ClientPacketHandlers::handleTakeScreenshotPacket);
        ClientPlayNetworking.registerGlobalReceiver(SpawnEntityPacket.CHANNEL,
                ClientPacketHandlers::handleEntitySpawnPacket);

        // Register entity renderers.
        EntityRendererRegistry.INSTANCE.register(ModEntityTypes.LINKING_BOOK, LinkingBookRenderer::new);
        if (Reference.isImmersivePortalsLoaded()) {
            ImmersivePortalsIntegration.registerEntityRenderingHandlers();
        }

        // Register block entity renderers/
        BlockEntityRendererRegistry.INSTANCE.register(ModBlockEntityTypes.LINKING_LECTERN, LinkingLecternRenderer::new);
        BlockEntityRendererRegistry.INSTANCE.register(ModBlockEntityTypes.LINK_TRANSLATOR, LinkTranslatorRenderer::new);
        BlockEntityRendererRegistry.INSTANCE.register(ModBlockEntityTypes.MARKER_SWITCH, MarkerSwitchRenderer::new);

        // Register screens.
        ScreenRegistry.register(ModScreenHandlerTypes.LINKING_BOOK, LinkingBookScreen::new);

        // Register ItemColors.
        ColorProviderRegistry.ITEM.register(LinkingBookItem::getColor, ModItems.BLANK_LINKING_BOOK);
        ColorProviderRegistry.ITEM.register(LinkingBookItem::getColor, ModItems.WRITTEN_LINKING_BOOK);

        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.LINKING_PORTAL, RenderLayer.getTranslucent());
    }

}
