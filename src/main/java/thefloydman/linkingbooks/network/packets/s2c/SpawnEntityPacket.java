/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.network.packets.s2c;

import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import thefloydman.linkingbooks.mixin.EntityInvoker;
import thefloydman.linkingbooks.util.Reference;

public class SpawnEntityPacket {

    public static final Identifier CHANNEL = new Identifier(Reference.MOD_ID, "spawn_entity");

    int typeId;
    int entityId;
    UUID uuid;
    double posX;
    double posY;
    double posZ;
    byte pitch;
    byte yaw;
    byte headYaw;
    int velX;
    int velY;
    int velZ;
    CompoundTag extraData;

    public SpawnEntityPacket(Entity entity) {
        this.typeId = Registry.ENTITY_TYPE.getRawId(entity.getType());
        this.entityId = entity.getEntityId();
        this.uuid = entity.getUuid();
        this.posX = entity.getX();
        this.posY = entity.getY();
        this.posZ = entity.getZ();
        this.pitch = (byte) MathHelper.floor(entity.pitch * 256.0F / 360.0F);
        this.yaw = (byte) MathHelper.floor(entity.yaw * 256.0F / 360.0F);
        this.headYaw = (byte) (entity.getHeadYaw() * 256.0F / 360.0F);
        Vec3d velocity = entity.getVelocity();
        double d1 = MathHelper.clamp(velocity.x, -3.9D, 3.9D);
        double d2 = MathHelper.clamp(velocity.y, -3.9D, 3.9D);
        double d3 = MathHelper.clamp(velocity.z, -3.9D, 3.9D);
        this.velX = (int) (d1 * 8000.0D);
        this.velY = (int) (d2 * 8000.0D);
        this.velZ = (int) (d3 * 8000.0D);
        this.extraData = new CompoundTag();
        ((EntityInvoker) entity).invokeWriteCustomDataToTag(this.extraData);
    }

    private SpawnEntityPacket(int typeId, int entityId, UUID uuid, double posX, double posY, double posZ, byte pitch,
            byte yaw, byte headYaw, int velX, int velY, int velZ, CompoundTag extraData) {
        this.typeId = typeId;
        this.entityId = entityId;
        this.uuid = uuid;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.pitch = pitch;
        this.yaw = yaw;
        this.headYaw = headYaw;
        this.velX = velX;
        this.velY = velY;
        this.velZ = velZ;
        this.extraData = extraData;
    }

    public PacketByteBuf toData(PacketByteBuf buffer) {
        buffer.writeVarInt(this.typeId);
        buffer.writeInt(this.entityId);
        buffer.writeUuid(this.uuid);
        buffer.writeDouble(this.posX);
        buffer.writeDouble(this.posY);
        buffer.writeDouble(this.posZ);
        buffer.writeByte(this.pitch);
        buffer.writeByte(this.yaw);
        buffer.writeByte(this.headYaw);
        buffer.writeShort(this.velX);
        buffer.writeShort(this.velY);
        buffer.writeShort(this.velZ);
        buffer.writeCompoundTag(this.extraData);
        return buffer;
    }

    public static SpawnEntityPacket fromData(PacketByteBuf buf) {
        SpawnEntityPacket packet = new SpawnEntityPacket(buf.readVarInt(), buf.readInt(), buf.readUuid(),
                buf.readDouble(), buf.readDouble(), buf.readDouble(), buf.readByte(), buf.readByte(), buf.readByte(),
                buf.readShort(), buf.readShort(), buf.readShort(), buf.readCompoundTag());
        return packet;
    }

}
