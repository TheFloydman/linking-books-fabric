/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.network.packets.s2c;

import java.util.UUID;

import com.mojang.blaze3d.systems.RenderSystem;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Util;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import thefloydman.linkingbooks.mixin.EntityInvoker;
import thefloydman.linkingbooks.network.packets.c2s.SaveLinkingPanelImagePacket;
import thefloydman.linkingbooks.util.ImageUtils;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.world.LinkingBooksPersistentState;

public class ClientPacketHandlers {

    @Environment(EnvType.CLIENT)
    public static void handleEntitySpawnPacket(MinecraftClient client, ClientPlayNetworkHandler handler,
            PacketByteBuf buffer, PacketSender responseSender) {

        SpawnEntityPacket packet = SpawnEntityPacket.fromData(buffer);

        client.execute(() -> {

            EntityType<?> type = Registry.ENTITY_TYPE.get(packet.typeId);
            if (type == null) {
                return;
            }
            Entity entity = type.create(client.world);
            if (entity == null) {
                return;
            }
            entity.updateTrackedPosition(packet.posX, packet.posY, packet.posZ);
            entity.refreshPositionAfterTeleport(packet.posX, packet.posY, packet.posZ);
            entity.pitch = (packet.pitch * 360) / 256.0F;
            entity.yaw = (packet.yaw * 360) / 256.0F;
            entity.setEntityId(packet.entityId);
            entity.setUuid(packet.uuid);
            entity.setVelocity(packet.velX / 8000.0, packet.velY / 8000.0, packet.velZ / 8000.0);
            ((EntityInvoker) entity).invokeReadCustomDataFromTag(packet.extraData);
            client.world.addEntity(packet.entityId, entity);
        });

    }

    @Environment(EnvType.CLIENT)
    public static void handleTakeScreenshotPacket(MinecraftClient client, ClientPlayNetworkHandler handler,
            PacketByteBuf buffer, PacketSender responseSender) {

        TakeScreenshotForLinkingBookPacket packet = TakeScreenshotForLinkingBookPacket.fromData(buffer);

        client.execute(() -> {
            if (RenderSystem.isOnRenderThread()) {
                ClientPacketHandlers.getScreenshot(packet.uuid);
            } else {
                RenderSystem.recordRenderCall(() -> {
                    ClientPacketHandlers.getScreenshot(packet.uuid);
                });
            }
        });
    }

    @Environment(EnvType.CLIENT)
    static void getScreenshot(UUID uuid) {

        MinecraftClient mc = MinecraftClient.getInstance();
        Framebuffer buffer = mc.getFramebuffer();
        float largeWidth = buffer.viewportWidth;
        float largeHeight = buffer.viewportHeight;
        float smallWidth = 64.0F;
        float smallHeight = 42.0F;
        if (largeWidth / largeHeight > smallWidth / smallHeight) {
            while (largeHeight % 42 != 0) {
                largeHeight--;
            }
            while (largeWidth / largeHeight != smallWidth / smallHeight) {
                largeWidth--;
            }
        } else if (largeWidth / largeHeight < smallWidth / smallHeight) {
            while (largeWidth % 64 != 0) {
                largeWidth--;
            }
            while (largeWidth / largeHeight != smallWidth / smallHeight) {
                largeHeight--;
            }
        }

        NativeImage fullImage = new NativeImage(buffer.textureWidth, buffer.textureHeight, false);
        buffer.beginWrite(true);
        boolean hide = mc.options.hudHidden;
        mc.options.hudHidden = true;
        mc.gameRenderer.renderWorld(mc.getTickDelta(), Util.getMeasuringTimeNano(), new MatrixStack());
        mc.options.hudHidden = hide;
        buffer.beginRead();
        fullImage.loadFromTextureImage(0, true);
        mc.getFramebuffer().endWrite();
        fullImage.mirrorVertically();
        NativeImage largeImage = new NativeImage((int) largeWidth, (int) largeHeight, false);
        int initialX = (int) ((buffer.viewportWidth - largeWidth) / 2);
        int initialY = (int) ((buffer.viewportHeight - largeHeight) / 2);
        for (int largeY = 0, fullY = initialY; largeY < largeHeight; largeY++, fullY++) {
            for (int largeX = 0, fullX = initialX; largeX < largeWidth; largeX++, fullX++) {
                largeImage.setPixelColor(largeX, largeY, fullImage.getPixelColor(fullX, fullY));
            }
        }
        NativeImage smallImage = new NativeImage((int) smallWidth, (int) smallHeight, false);
        largeImage.resizeSubRectTo(0, 0, (int) largeWidth, (int) largeHeight, smallImage);
        largeImage.close();
        fullImage.close();
        CompoundTag imageTag = ImageUtils.imageToNBT(smallImage);
        if (MinecraftClient.getInstance().isIntegratedServerRunning()) {
            LinkingBooksPersistentState persistentState = MinecraftClient.getInstance().getServer()
                    .getWorld(World.OVERWORLD).getPersistentStateManager()
                    .getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
            persistentState.addLinkingPanelImage(uuid, imageTag);
        } else {
            ClientPlayNetworking.send(SaveLinkingPanelImagePacket.CHANNEL,
                    new SaveLinkingPanelImagePacket(imageTag, uuid).toData(new PacketByteBuf(Unpooled.buffer())));
        }
    }

}
