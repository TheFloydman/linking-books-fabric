/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.network.packets.c2s;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.util.Reference;

public class LinkPacket {

    public static final Identifier CHANNEL = new Identifier(Reference.MOD_ID, "link");

    public boolean holdingBook = false;
    public LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());

    public LinkPacket(boolean holdingBook, LinkDataComponent linkData) {
        this.holdingBook = holdingBook;
        this.linkData = linkData;
    }

    public LinkPacket() {
        this(false, ModComponents.ITEM_LINK_DATA.get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack()));
    }

    public PacketByteBuf toData(PacketByteBuf buffer) {
        buffer.writeBoolean(this.holdingBook);
        this.linkData.writeToPacketByteBuf(buffer);
        return buffer;
    }

    public static LinkPacket fromData(PacketByteBuf buffer) {
        boolean holdingBook = buffer.readBoolean();
        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
        linkData.readFromPacketByteBuf(buffer);
        return new LinkPacket(holdingBook, linkData);
    }

}
