/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.network.packets.c2s;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;
import thefloydman.linkingbooks.util.LinkingUtils;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.world.LinkingBooksPersistentState;

public class ServerPacketHandlers {

    @Environment(EnvType.SERVER)
    public static void handleLinkPacketonServer(MinecraftServer server, ServerPlayerEntity player,
            ServerPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {

        LinkPacket packet = LinkPacket.fromData(buffer);

        server.execute(() -> {
            LinkingUtils.linkEntity(player, packet.linkData, packet.holdingBook);
        });
    }

    @Environment(EnvType.SERVER)
    public static void handleSaveImagePacketonServer(MinecraftServer server, ServerPlayerEntity player,
            ServerPlayNetworkHandler handler, PacketByteBuf buffer, PacketSender responseSender) {

        SaveLinkingPanelImagePacket packet = SaveLinkingPanelImagePacket.fromData(buffer);

        server.execute(() -> {
            LinkingBooksPersistentState persistentState = player.getServer().getWorld(World.OVERWORLD)
                    .getPersistentStateManager().getOrCreate(LinkingBooksPersistentState::new, Reference.MOD_ID);
            persistentState.addLinkingPanelImage(packet.uuid, packet.imageTag);
        });
    }

}
