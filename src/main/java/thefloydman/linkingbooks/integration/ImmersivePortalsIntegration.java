/*******************************************************************************
 * Linking Books
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.integration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.qouteall.immersive_portals.ClientWorldLoader;
import com.qouteall.immersive_portals.chunk_loading.ChunkVisibilityManager;
import com.qouteall.immersive_portals.chunk_loading.ChunkVisibilityManager.ChunkLoader;
import com.qouteall.immersive_portals.chunk_loading.DimensionalChunkPos;
import com.qouteall.immersive_portals.chunk_loading.NewChunkTrackingGraph;
import com.qouteall.immersive_portals.portal.PortalManipulation;
import com.qouteall.immersive_portals.portal.nether_portal.BlockPortalShape;
import com.qouteall.immersive_portals.render.GuiPortalRendering;
import com.qouteall.immersive_portals.render.MyRenderHelper;
import com.qouteall.immersive_portals.render.PortalEntityRenderer;
import com.qouteall.immersive_portals.render.context_management.WorldRenderInfo;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Direction.Axis;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.block.entity.LinkTranslatorBlockEntity;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.config.ModConfig;
import thefloydman.linkingbooks.entity.LinkingPortalEntity;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.util.Reference.EntityNames;

public class ImmersivePortalsIntegration {

    private static Map<UUID, ChunkLoader> chunkLoaders = new HashMap<UUID, ChunkLoader>();
    public static EntityType<LinkingPortalEntity> linkingPortalEntityType;

    public static void addChunkLoader(LinkDataComponent linkData, ServerPlayerEntity player) {
        removeChunkLoader(linkData, player);
        ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
        ChunkLoader chunkLoader = new ChunkVisibilityManager.ChunkLoader(
                new DimensionalChunkPos(RegistryKey.of(Registry.DIMENSION, linkData.getDimension()),
                        new ChunkPos(linkData.getPosition())),
                config.linkingPanelChunkLoadRadius);
        chunkLoaders.put(linkData.getUUID(), chunkLoader);
        NewChunkTrackingGraph.addPerPlayerAdditionalChunkLoader(player, chunkLoader);
    }

    public static void removeChunkLoader(LinkDataComponent linkData, ServerPlayerEntity player) {
        ChunkLoader chunkLoader = chunkLoaders.remove(linkData.getUUID());
        if (chunkLoader != null) {
            NewChunkTrackingGraph.removePerPlayerAdditionalChunkLoader(player, chunkLoader);
        }
    }

    @Environment(EnvType.CLIENT)
    public static void renderGuiPortal(LinkDataComponent linkData, Framebuffer frameBuffer, MinecraftClient client,
            MatrixStack matrixStack, int x, int y, int width, int height) {
        ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
        Matrix4f cameraTransformation = new Matrix4f();
        cameraTransformation.loadIdentity();
        cameraTransformation.multiply(Vector3f.POSITIVE_Y.getDegreesQuaternion(linkData.getRotation() + 180.0F));
        WorldRenderInfo worldRenderInfo = new WorldRenderInfo(
                ClientWorldLoader.getWorld(RegistryKey.of(Registry.DIMENSION, linkData.getDimension())),
                new Vec3d(linkData.getPosition().getX() + 0.5D, linkData.getPosition().getY() + 1.5D,
                        linkData.getPosition().getZ() + 0.5D),
                cameraTransformation, null, config.linkingPanelChunkRenderDistance, true);
        GuiPortalRendering.submitNextFrameRendering(worldRenderInfo, frameBuffer);
        MyRenderHelper.drawFramebuffer(frameBuffer, false, false, x * (float) client.getWindow().getScaleFactor(),
                (x + width) * (float) client.getWindow().getScaleFactor(),
                y * (float) client.getWindow().getScaleFactor(),
                (y + height) * (float) client.getWindow().getScaleFactor());
    }

    public static UUID[] addImmersivePortal(World world, double[] pos, double width, double height,
            Set<BlockPos> coveredBlocks, Axis axis, LinkDataComponent linkData, LinkTranslatorBlockEntity blockEntity) {
        if (axis == Axis.X) {
            axis = Axis.Z;
        } else if (axis == Axis.Z) {
            axis = Axis.X;
        }
        ItemStack stack = ModItems.WRITTEN_LINKING_BOOK.getDefaultStack();
        LinkDataComponent itemData = ModComponents.ITEM_LINK_DATA.get(stack);
        itemData.setDimension(linkData.getDimension());
        itemData.setLinkEffects(linkData.getLinkEffects());
        itemData.setPosition(linkData.getPosition());
        itemData.setRotation(linkData.getRotation());
        itemData.setUUID(linkData.getUUID());
        LinkingPortalEntity portal = new LinkingPortalEntity(linkingPortalEntityType, world, stack,
                blockEntity.getPos());
        portal.setPos(pos[0], pos[1], pos[2]);
        BlockPortalShape shape = new BlockPortalShape(coveredBlocks, axis);
        shape.initPortalPosAxisShape(portal, false);
        PortalManipulation.setPortalTransformation(portal, RegistryKey.of(Registry.DIMENSION, linkData.getDimension()),
                new Vec3d(linkData.getPosition().getX() + 0.5D,
                        linkData.getPosition().getY() + (height / 2.0D) + (axis == Axis.Y ? 2.0D : 0.0D) + 0.5D,
                        linkData.getPosition().getZ() + 0.5D),
                null, 1.0D);
        PortalManipulation.removeOverlappedPortals(world, portal.getPos(), portal.getNormal(), (p) -> {
            return p instanceof LinkingPortalEntity;
        }, (p) -> {
        });
        world.spawnEntity(portal);
        LinkingPortalEntity reversePortal = PortalManipulation.createFlippedPortal(portal, linkingPortalEntityType);
        reversePortal.setBlockEntityPos(portal.getBlockEntityPos());
        PortalManipulation.removeOverlappedPortals(world, reversePortal.getPos(), reversePortal.getNormal(), (p) -> {
            return p instanceof LinkingPortalEntity;
        }, (p) -> {
        });
        world.spawnEntity(reversePortal);
        return new UUID[] { portal.getUuid(), reversePortal.getUuid() };
    }

    public static void registerImmersivePortalsEntities() {
        linkingPortalEntityType = FabricEntityTypeBuilder
                .<LinkingPortalEntity>create(SpawnGroup.MISC, LinkingPortalEntity::new)
                .dimensions(new EntityDimensions(1, 1, true)).fireImmune().trackRangeBlocks(96)
                .forceTrackedVelocityUpdates(true).trackedUpdateRate(2).build();
        Registry.register(Registry.ENTITY_TYPE, EntityNames.LINKING_PORTAL, linkingPortalEntityType);
    }

    public static void registerEntityRenderingHandlers() {
        EntityRendererRegistry.INSTANCE.register(linkingPortalEntityType,
                (entityRenderDispatcher, context) -> new PortalEntityRenderer(entityRenderDispatcher));
    }

    public static List<LinkingPortalEntity> getNearbyLinkingPortals(BlockPos pos, World world) {
        return world.getEntitiesByClass(LinkingPortalEntity.class,
                new Box(pos.down(64).south(64).west(64), pos.up(64).north(64).east(64)), null);
    }

    public static void deleteLinkingPortals(LinkTranslatorBlockEntity blockEntity) {
        List<LinkingPortalEntity> nearbyPortals = getNearbyLinkingPortals(blockEntity.getPos(), blockEntity.getWorld());
        for (LinkingPortalEntity portal : nearbyPortals) {
            if (portal.getBlockEntityPos().equals(blockEntity.getPos())) {
                portal.remove();
            }
        }
    }

}