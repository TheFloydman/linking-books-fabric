/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.util;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;

public class Reference {

    public static final String MOD_ID = "linkingbooks";
    public static MinecraftServer server = null;

    /**
     * Convenience method to make a ResourceLocation under this mod's domain.
     * 
     * @param path The path of the ResourceLocation.
     * @return A ResourceLocation with this mod's ID as the domain and the given
     *         parameter as the path.
     */
    public static Identifier getAsResourceLocation(String path) {
        return new Identifier(MOD_ID, path);
    }

    public static boolean isModLoaded(String modId) {
        return FabricLoader.getInstance().isModLoaded(modId);
    }

    public static boolean isImmersivePortalsLoaded() {
        return isModLoaded("immersive_portals");
    }

    public static class BlockNames {
        public static final Identifier INK = getAsResourceLocation("ink");
        public static final Identifier LINKING_LECTERN = getAsResourceLocation("linking_lectern");
        public static final Identifier MARKER_SWITCH = getAsResourceLocation("marker_switch");
        public static final Identifier NARA = getAsResourceLocation("nara");
        public static final Identifier LINK_TRANSLATOR = getAsResourceLocation("link_translator");
        public static final Identifier LINKING_PORTAL = getAsResourceLocation("linking_portal");
        public static final Identifier BOOKSHELF_STAIRS = getAsResourceLocation("bookshelf_stairs");
    }

    public static class ItemNames {
        public static final Identifier INK_BUCKET = getAsResourceLocation("ink_bucket");
        public static final Identifier BLANK_LINKNG_BOOK = getAsResourceLocation("blank_linking_book");
        public static final Identifier WRITTEN_LINKNG_BOOK = getAsResourceLocation("written_linking_book");
        public static final Identifier LINKING_PANEL = getAsResourceLocation("linking_panel");
    }

    public static class EntityNames {
        public static final Identifier LINKING_BOOK = getAsResourceLocation("linking_book");
        public static final Identifier LINKING_PORTAL = getAsResourceLocation("linking_portal");
    }

    public static class BlockEntityNames {
        public static final Identifier LINKING_LECTERN = getAsResourceLocation("linking_lectern");
        public static final Identifier LINK_TRANSLATOR = getAsResourceLocation("link_translator");
        public static final Identifier MARKER_SWITCH = getAsResourceLocation("marker_switch");
    }

    public static class FluidNames {
        public static final Identifier INK = getAsResourceLocation("ink");
        public static final Identifier FLOWING_INK = getAsResourceLocation("flowing_ink");
    }

    public static class ContainerNames {
        public static final Identifier LINKING_BOOK = getAsResourceLocation("linking_book");
    }

    public static class CapabilityNames {
        public static final Identifier LINK_DATA = getAsResourceLocation("link_data");
    }

    public static class RecipeSerializerNames {
        public static final String BLANK_LINKING_BOOK = MOD_ID + ":blank_linking_book";
        public static final String LINK_EFFECT = MOD_ID + ":link_effect";
    }

    public static class LinkEffectNames {
        public static final Identifier POISON_EFFECT = getAsResourceLocation("poison_effect");
        public static final Identifier INTRAAGE_LINKING = getAsResourceLocation("intraage_linking");
        public static final Identifier TETHERED = getAsResourceLocation("tethered");
    }

    public static class ComponentNames {
        public static final Identifier COLOR = getAsResourceLocation("color");
        public static final Identifier LINK_DATA = getAsResourceLocation("link_data");
    }

    public static class Resources {
        public static final Identifier INK_TEXTURE = getAsResourceLocation("block/ink_still");
        public static final Identifier FLOWING_INK_TEXTURE = getAsResourceLocation("block/ink_flow");
        public static final Identifier LINKING_BOOK_ENTITY_TEXTURE = getAsResourceLocation(
                "textures/entity/linking_book.png");
    }

}
