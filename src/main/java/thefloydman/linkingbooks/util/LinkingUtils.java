/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.util;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.buffer.Unpooled;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.config.ModConfig;
import thefloydman.linkingbooks.entity.LinkingBookEntity;
import thefloydman.linkingbooks.item.BlankLinkingBookItem;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.network.packets.s2c.TakeScreenshotForLinkingBookPacket;

public class LinkingUtils {

    private static final Logger LOGGER = LogManager.getLogger();

    public static ItemStack createWrittenLinkingBookfromBlankLinkingBook(PlayerEntity player, ItemStack originItem) {

        if (!(originItem.getItem() instanceof BlankLinkingBookItem)) {
            return originItem;
        }

        ItemStack resultItem = ModItems.WRITTEN_LINKING_BOOK.getDefaultStack();

        ColorComponent originColor = ModComponents.ITEM_COLOR.get(originItem);
        ColorComponent resultColor = ModComponents.ITEM_COLOR.get(resultItem);
        resultColor.setColor(originColor.getColor());

        LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA.get(resultItem);
        linkData.setDimension(player.getEntityWorld().getRegistryKey().getValue());
        linkData.setPosition(player.getBlockPos());
        linkData.setRotation(player.yaw);
        linkData.addLinkEffect(LinkEffects.DUMMY);

        if (player instanceof ServerPlayerEntity) {
            ServerPlayNetworking.send((ServerPlayerEntity) player, TakeScreenshotForLinkingBookPacket.CHANNEL,
                    new TakeScreenshotForLinkingBookPacket(linkData.getUUID())
                            .toData(new PacketByteBuf(Unpooled.buffer())));
        }

        return resultItem;
    }

    /**
     * Teleport an entity to a dimension and position. Should only be called
     * server-side (dedicated or integrated).
     */
    public static boolean linkEntity(Entity entity, LinkDataComponent linkData, boolean holdingBook) {

        World world = entity.getEntityWorld();

        if (world.isClient()) {
            LOGGER.info(
                    "An attempt has been made to directly link an entity from the client. Only do this from the server.");
        } else if (linkData == null) {
            LOGGER.info("A null ILinkInfo has been supplied. Link failed.");
        } else if (linkData.getDimension() == null) {
            LOGGER.info("ILinkInfo.getDimension() returned null. Link failed.");
        } else if (linkData.getPosition() == null) {
            LOGGER.info("ILinkInfo.getPosition() returned null. Link failed.");
        } else if (!linkData.getLinkEffects().contains(LinkEffects.INTRAAGE_LINKING)
                && world.getRegistryKey().getValue().equals(linkData.getDimension())) {
            if (entity instanceof ServerPlayerEntity) {
                ServerPlayerEntity player = (ServerPlayerEntity) entity;
                player.closeHandledScreen();
                player.closeScreenHandler();
                player.sendMessage(new TranslatableText("message.linkingbooks.no_intraage_linking"), true);
            }
        } else {

            ServerWorld serverWorld = world.getServer()
                    .getWorld(RegistryKey.of(Registry.DIMENSION, linkData.getDimension()));

            if (serverWorld == null) {
                LOGGER.info("Cannot find dimension \"" + linkData.getDimension().toString() + "\". Link failed.");
                return false;
            }

            for (LinkEffect effect : linkData.getLinkEffects()) {
                if (!effect.canStartLink(entity, linkData)) {
                    if (entity instanceof ServerPlayerEntity) {
                        ServerPlayerEntity player = (ServerPlayerEntity) entity;
                        player.closeHandledScreen();
                        player.closeScreenHandler();
                        player.sendMessage(new TranslatableText("message.linkingbooks.link_failed_start"), true);
                    }
                    return false;
                }
            }

            for (LinkEffect effect : linkData.getLinkEffects()) {
                effect.onLinkStart(entity, linkData);
            }

            Vec3d originalPos = entity.getPos();
            float originalRot = entity.yaw;
            BlockPos pos = linkData.getPosition();
            double x = pos.getX() + 0.5D;
            double y = pos.getY();
            double z = pos.getZ() + 0.5D;
            float rotation = linkData.getRotation();
            boolean tookExperience = false;

            /*
             * TODO: Find a way to teleport without client moving entity model through
             * world.
             */

            if (entity instanceof ServerPlayerEntity) {
                ServerPlayerEntity player = (ServerPlayerEntity) entity;
                // Deduct experience levels if a cost has been set in config.
                if (!player.isCreative()) {
                    ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
                    if (player.experienceLevel < config.linkingCostExperienceLevels) {
                        player.closeHandledScreen();
                        player.closeScreenHandler();
                        player.sendMessage(new TranslatableText("message.linkingbooks.insufficient_experience"), true);
                        return false;
                    } else {
                        player.addExperienceLevels(config.linkingCostExperienceLevels * -1);
                        tookExperience = true;
                    }
                }
                world.getServer().execute(() -> {
                    if (holdingBook && !linkData.getLinkEffects().contains(LinkEffects.TETHERED)) {
                        LinkingBookEntity book = new LinkingBookEntity(world, player.getMainHandStack().copy());
                        Vec3d lookVec = player.getRotationVector();
                        book.updatePositionAndAngles(player.getX() + (lookVec.getX() / 4.0D), player.getY() + 1.0D,
                                player.getZ() + (lookVec.getZ() / 4.0D), player.headYaw, 0.0F);
                        world.spawnEntity(book);
                        player.getMainHandStack().decrement(1);
                    }
                    player.closeScreenHandler();
                    player.closeHandledScreen();
                    player.teleport(serverWorld, x, y, z, rotation, player.pitch);
                });
            } else {
                world.getServer().execute(() -> {
                    CompoundTag tag = new CompoundTag();
                    entity.saveSelfToTag(tag);
                    entity.remove();
                    Entity entityCopy = EntityType.getEntityFromTag(tag, serverWorld).orElse(null);
                    if (entityCopy != null) {
                        entityCopy.updatePosition(x, y, z);
                        serverWorld.spawnEntity(entityCopy);
                        serverWorld.onDimensionChanged(entityCopy);
                    }
                });
            }
            for (LinkEffect effect : linkData.getLinkEffects()) {
                if (!effect.canFinishLink(entity, linkData)) {
                    if (entity instanceof ServerPlayerEntity) {
                        ServerPlayerEntity player = (ServerPlayerEntity) entity;
                        if (tookExperience) {
                            ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
                            player.addExperienceLevels(config.linkingCostExperienceLevels);
                        }
                        serverWorld.getServer().execute(() -> {
                            player.teleport((ServerWorld) world, originalPos.x, originalPos.y, originalPos.z,
                                    originalRot, player.pitch);
                            player.sendMessage(new TranslatableText("message.linkingbooks.link_failed_end"), true);
                        });
                    } else {
                        serverWorld.getServer().execute(() -> {
                            CompoundTag tag = new CompoundTag();
                            entity.saveSelfToTag(tag);
                            entity.remove();
                            Entity entityCopy = EntityType.getEntityFromTag(tag, world).orElse(null);
                            if (entityCopy != null) {
                                entityCopy.updatePosition(originalPos.x, originalPos.y, originalPos.z);
                                world.spawnEntity(entityCopy);
                                ((ServerWorld) world).onDimensionChanged(entityCopy);
                            }
                        });
                    }
                    return false;
                }
            }
            for (LinkEffect effect : linkData.getLinkEffects()) {
                effect.onLinkEnd(entity, linkData);
            }
            return true;
        }
        return false;
    }

    /**
     * Teleport multiple entities to a dimension and position using the same
     * ILinkInfo. Should only be called server-side.
     * 
     * @param entities
     * @param linkInfo
     * @return The number of entities that were successfully teleported.
     */
    public static int linkEntities(List<Entity> entities, LinkDataComponent linkInfo, boolean holdingBook) {
        int linked = 0;
        for (Entity entity : entities) {
            linked += linkEntity(entity, linkInfo, holdingBook) == true ? 1 : 0;
        }
        return linked;
    }

}
