/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.gui.screen;

import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import thefloydman.linkingbooks.client.gui.widget.LinkingBookWidget;
import thefloydman.linkingbooks.client.gui.widget.NestedWidget;
import thefloydman.linkingbooks.screen.LinkingBookScreenHandler;

public class LinkingBookScreen extends HandledScreen<LinkingBookScreenHandler> {

    public LinkingBookScreen(LinkingBookScreenHandler container, PlayerInventory inventory, Text narration) {
        super(container, inventory, narration);
        this.backgroundWidth = 256;
        this.backgroundHeight = 192;
    }

    @Override
    protected void init() {
        super.init();
        NestedWidget linkingBook = this.addButton(new LinkingBookWidget((this.width - 256) / 2, (this.height - 192) / 2,
                0.0F, 256, 192, new LiteralText("Linking Book"), this.getScreenHandler().holdingBook,
                this.getScreenHandler().bookColor, this.getScreenHandler().linkData, this.getScreenHandler().canLink,
                this.getScreenHandler().linkingPanelImage));
        linkingBook.addListener(this);
    }

    @Override
    protected void drawBackground(MatrixStack matrixStack, float partialTicks, int x, int y) {
    }

    @Override
    protected void drawForeground(MatrixStack matrixStack, int x, int y) {
    }

}
