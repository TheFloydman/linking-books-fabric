/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.gui.widget;

import java.awt.Color;

import com.mojang.blaze3d.platform.GlStateManager.DstFactor;
import com.mojang.blaze3d.platform.GlStateManager.SrcFactor;
import com.mojang.blaze3d.systems.RenderSystem;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.Element;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.util.ColorUtils;
import thefloydman.linkingbooks.util.Reference;

@Environment(EnvType.CLIENT)
public class LinkingBookWidget extends NestedWidget {

    private static final Identifier COVER_TEXTURE = new Identifier(Reference.MOD_ID,
            "textures/gui/linkingbook/linking_book_cover.png");
    private static final Identifier PAPER_TEXTURE = new Identifier(Reference.MOD_ID,
            "textures/gui/linkingbook/linking_book_paper.png");

    public int color = ColorUtils.dyeColorAsInt(DyeColor.GREEN);

    public LinkingBookWidget(int x, int y, float zLevel, int width, int height, Text narration, boolean holdingBook,
            int color, LinkDataComponent linkData, boolean canLink, CompoundTag linkingPanelImage) {
        super(x, y, width, height, narration);
        this.color = color;
        NestedWidget linkingPanel = this.addChild(new LinkingPanelWidget(this.x + 155, this.y + 41, 0.0F, 64, 42,
                new LiteralText("Linking Panel"), holdingBook, linkData, canLink, linkingPanelImage));
        for (Element listener : this.listeners) {
            linkingPanel.addListener(listener);
        }
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        if (!this.visible) {
            return;
        }
        matrixStack.push();
        RenderSystem.pushMatrix();

        RenderSystem.blendFuncSeparate(SrcFactor.SRC_ALPHA, DstFactor.ONE_MINUS_SRC_ALPHA, SrcFactor.ONE,
                DstFactor.ZERO);
        MinecraftClient.getInstance().getTextureManager().bindTexture(COVER_TEXTURE);
        float[] color = new Color(this.color).getRGBColorComponents(null);
        RenderSystem.color4f(MathHelper.clamp(color[0], 0.1F, 1.0F), MathHelper.clamp(color[1], 0.1F, 1.0F),
                MathHelper.clamp(color[2], 0.1F, 1.0F), 1.0F);
        this.drawTexture(matrixStack, this.x, this.y, 0, 0, this.width, this.height);

        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        MinecraftClient.getInstance().getTextureManager().bindTexture(PAPER_TEXTURE);
        this.drawTexture(matrixStack, this.x, this.y, 0, 0, this.width, this.height);

        RenderSystem.popMatrix();
        matrixStack.pop();

        this.renderChildren(matrixStack, mouseX, mouseY, partialTicks);
    }

}
