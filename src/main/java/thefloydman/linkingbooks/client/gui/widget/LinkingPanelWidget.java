/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.gui.widget;

import java.awt.Color;

import com.mojang.blaze3d.systems.RenderSystem;

import io.netty.buffer.Unpooled;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.texture.NativeImageBackedTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.config.ModConfig;
import thefloydman.linkingbooks.integration.ImmersivePortalsIntegration;
import thefloydman.linkingbooks.network.packets.c2s.LinkPacket;
import thefloydman.linkingbooks.util.ImageUtils;
import thefloydman.linkingbooks.util.LinkingUtils;
import thefloydman.linkingbooks.util.Reference;

@Environment(EnvType.CLIENT)
public class LinkingPanelWidget extends NestedWidget {

    public boolean holdingBook = false;
    public LinkDataComponent linkData;
    public boolean canLink = false;
    NativeImageBackedTexture linkingPanelImage = null;
    private Framebuffer frameBuffer = new Framebuffer(64, 42, true, true);
    MinecraftClient client = MinecraftClient.getInstance();
    long openTime;

    public LinkingPanelWidget(int x, int y, float zLevel, int width, int height, Text narration, boolean holdingBook,
            LinkDataComponent linkData, boolean canLink, CompoundTag linkingPanelImageTag) {
        super(x, y, width, height, narration);
        this.holdingBook = holdingBook;
        this.linkData = linkData;
        this.canLink = canLink;
        if (linkingPanelImageTag != null && !linkingPanelImageTag.isEmpty()) {
            NativeImage linkingPanelImage = ImageUtils.imageFromNBT(linkingPanelImageTag);
            NativeImage image256 = new NativeImage(256, 256, false);
            for (int textureY = 0; textureY < linkingPanelImage.getHeight(); textureY++) {
                for (int textureX = 0; textureX < linkingPanelImage.getWidth(); textureX++) {
                    image256.setPixelColor(textureX, textureY, linkingPanelImage.getPixelColor(textureX, textureY));
                }
            }
            this.linkingPanelImage = new NativeImageBackedTexture(image256);
        }
        this.openTime = this.client.world.getTime();
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        if (!this.visible) {
            return;
        }
        int panelColor = this.canLink ? new Color(32, 192, 255).getRGB() : new Color(192, 192, 192).getRGB();
        this.zFill(matrixStack, this.x, this.y, this.x + this.width, this.y + this.height, panelColor);

        if (this.canLink) {
            ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
            if (Reference.isImmersivePortalsLoaded() && config.useImmersivePortalsForLinkingPanels) {
                ImmersivePortalsIntegration.renderGuiPortal(this.linkData, this.frameBuffer, this.client, matrixStack,
                        this.x, this.y, this.width, this.height);
                float timeDelta = this.client.world.getTime() - this.openTime;
                if (timeDelta < 50.0F) {
                    RenderSystem.pushMatrix();
                    RenderSystem.enableAlphaTest();
                    this.zFill(matrixStack, this.x, this.y, this.x + this.width, this.y + this.height,
                            new Color(0.0F, 0.0F, 0.0F, 1.0F - (timeDelta / 50.0F)).getRGB());
                    RenderSystem.disableAlphaTest();
                    RenderSystem.popMatrix();
                }
            } else {
                if (this.linkingPanelImage != null) {
                    RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
                    this.linkingPanelImage.bindTexture();
                    this.drawTexture(matrixStack, this.x, this.y, 0, 0, this.linkingPanelImage.getImage().getWidth(),
                            this.linkingPanelImage.getImage().getHeight());
                }
            }
        }

        this.renderChildren(matrixStack, mouseX, mouseY, partialTicks);

    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (this.isInside(mouseX, mouseY)) {
            if (MinecraftClient.getInstance().isIntegratedServerRunning()) {
                ServerPlayerEntity player = MinecraftClient.getInstance().getServer().getPlayerManager()
                        .getPlayer(this.client.player.getUuid());
                LinkingUtils.linkEntity(player, this.linkData, this.holdingBook);
            } else {
                ClientPlayNetworking.send(LinkPacket.CHANNEL,
                        new LinkPacket(this.holdingBook, this.linkData).toData(new PacketByteBuf(Unpooled.buffer())));
            }
            return true;
        }
        return this.onMouseClickChildren(mouseX, mouseY, button);
    }

}
