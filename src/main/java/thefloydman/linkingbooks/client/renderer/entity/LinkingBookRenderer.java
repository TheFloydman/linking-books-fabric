/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.renderer.entity;

import java.awt.Color;

import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry.Context;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.client.renderer.entity.model.LinkingBookCoverModel;
import thefloydman.linkingbooks.client.renderer.entity.model.LinkingBookPagesModel;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.entity.LinkingBookEntity;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.util.Reference.Resources;

public class LinkingBookRenderer extends EntityRenderer<LinkingBookEntity> {

    private LinkingBookCoverModel coverModel = new LinkingBookCoverModel();
    private LinkingBookPagesModel pagesModel = new LinkingBookPagesModel();
    private float[] color = { 0.0F, 1.0F, 0.0F };

    public LinkingBookRenderer(EntityRenderDispatcher renderManager, Context context) {
        super(renderManager);
        this.coverModel.setBookState(0.9F);
        this.pagesModel.setBookState(0.9F);
    }

    @Override
    public void render(LinkingBookEntity entity, float yaw, float partialTicks, MatrixStack matrixStack,
            VertexConsumerProvider buffer, int packedLight) {

        ItemStack bookStack = entity.getItem();
        if (bookStack != null && !bookStack.isEmpty()) {
            Item item = bookStack.getItem();
            if (item != null && item instanceof WrittenLinkingBookItem) {
                ColorComponent color = ModComponents.ITEM_COLOR.get(bookStack);
                this.color = new Color(color.getColor()).getRGBColorComponents(this.color);
            }
        }

        matrixStack.push();

        matrixStack.scale(0.75F, 0.75F, 0.75F);
        matrixStack.multiply(Vector3f.POSITIVE_X.getRadialQuaternion((float) Math.PI));
        matrixStack.multiply(Vector3f.POSITIVE_Y
                .getRadialQuaternion((yaw / 360.0F * (float) Math.PI * 2.0F) - ((float) Math.PI / 2.0F)));
        matrixStack.multiply(Vector3f.POSITIVE_Z.getRadialQuaternion((float) Math.PI / 2 * 3));

        VertexConsumer vertexBuilder = buffer
                .getBuffer(this.coverModel.getLayer(Resources.LINKING_BOOK_ENTITY_TEXTURE));

        if (entity.hurtTime > 0) {
            this.coverModel.render(matrixStack, vertexBuilder, packedLight, OverlayTexture.DEFAULT_UV, 0.7F, 0.0F, 0.0F,
                    0.4F);
            this.pagesModel.render(matrixStack, vertexBuilder, packedLight, OverlayTexture.DEFAULT_UV, 0.7F, 0.0F, 0.0F,
                    0.4F);
        } else {
            this.coverModel.render(matrixStack, vertexBuilder, packedLight, OverlayTexture.DEFAULT_UV, this.color[0],
                    this.color[1], this.color[2], 1.0F);
            this.pagesModel.render(matrixStack, vertexBuilder, packedLight, OverlayTexture.DEFAULT_UV, 1.0F, 1.0F, 1.0F,
                    1.0F);
        }

        matrixStack.pop();
    }

    @Override
    public Identifier getTexture(LinkingBookEntity entity) {
        return Resources.LINKING_BOOK_ENTITY_TEXTURE;
    }

    @Override
    protected boolean hasLabel(LinkingBookEntity entity) {
        return super.hasLabel(entity)
                && (entity.shouldRenderName() || entity.hasCustomName() && entity == this.dispatcher.targetedEntity);
    }

}