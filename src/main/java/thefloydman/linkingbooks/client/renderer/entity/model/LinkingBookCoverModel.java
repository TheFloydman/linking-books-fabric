/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.renderer.entity.model;

import java.util.Arrays;
import java.util.List;

import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.MathHelper;
import thefloydman.linkingbooks.entity.LinkingBookEntity;

public class LinkingBookCoverModel extends EntityModel<LinkingBookEntity> {

    private final ModelPart coverRight = new ModelPart(64, 32, 0, 0).addCuboid(-6.0F, -5.0F, -0.005F, 6.0F, 10.0F,
            0.005F);
    private final ModelPart coverLeft = new ModelPart(64, 32, 16, 0).addCuboid(0.0F, -5.0F, -0.005F, 6.0F, 10.0F,
            0.005F);
    private final ModelPart bookSpine = new ModelPart(64, 32, 12, 0).addCuboid(-1.0F, -5.0F, 0.0F, 2.0F, 10.0F, 0.005F);
    private final List<ModelPart> allModels = Arrays.asList(this.coverRight, this.coverLeft, this.bookSpine);

    public LinkingBookCoverModel() {
        this.coverRight.setPivot(0.0F, 0.0F, -1.0F);
        this.coverLeft.setPivot(0.0F, 0.0F, 1.0F);
        this.bookSpine.yaw = ((float) Math.PI / 2F);
    }

    @Override
    public void render(MatrixStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn,
            float red, float green, float blue, float alpha) {
        this.allModels.forEach((model) -> {
            model.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        });
    }

    public void setBookState(float openAmount) {
        float radians = MathHelper.clamp((float) Math.PI / 2.0F * openAmount, 0.0F, (float) Math.PI / 2.0F);
        this.coverRight.yaw = (float) Math.PI + radians;
        this.coverLeft.yaw = -radians;
    }

    @Override
    public void setAngles(LinkingBookEntity arg0, float arg1, float arg2, float arg3, float arg4, float arg5) {
    }

}