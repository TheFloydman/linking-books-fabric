/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.renderer.blockentity;

import java.awt.Color;

import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.block.LinkingLecternBlock;
import thefloydman.linkingbooks.block.entity.LinkingLecternBlockEntity;
import thefloydman.linkingbooks.client.renderer.entity.model.LinkingBookCoverModel;
import thefloydman.linkingbooks.client.renderer.entity.model.LinkingBookPagesModel;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.WrittenLinkingBookItem;
import thefloydman.linkingbooks.util.Reference.Resources;

public class LinkingLecternRenderer extends BlockEntityRenderer<LinkingLecternBlockEntity> {

    private LinkingBookCoverModel coverModel = new LinkingBookCoverModel();
    private LinkingBookPagesModel pagesModel = new LinkingBookPagesModel();
    private float[] color = { 1.0F, 1.0F, 1.0F };

    public LinkingLecternRenderer(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
        this.coverModel.setBookState(0.95F);
        this.pagesModel.setBookState(0.95F);
    }

    @Override
    public void render(LinkingLecternBlockEntity tileEntity, float arg1, MatrixStack matrixStack,
            VertexConsumerProvider buffer, int arg4, int arg5) {
        if (tileEntity.hasBook()) {

            ItemStack bookStack = tileEntity.getBook();
            if (bookStack != null && !bookStack.isEmpty()) {
                Item item = bookStack.getItem();
                if (item != null && item instanceof WrittenLinkingBookItem) {
                    ColorComponent color = ModComponents.ITEM_COLOR.get(bookStack);
                    if (color != null) {
                        this.color = new Color(color.getColor()).getRGBColorComponents(this.color);
                    }
                }
            }

            matrixStack.push();

            float rotation = 0.0F;
            double[] translate = { 0.0D, 0.0D, 0.0D };
            switch (tileEntity.getCachedState().get(LinkingLecternBlock.FACING)) {
            case NORTH:
                rotation = 1.0F;
                translate[0] = 0.5D;
                translate[1] = 1.0D;
                translate[2] = 0.4D;
                break;
            case WEST:
                rotation = 2.0F;
                translate[0] = 0.4D;
                translate[1] = 1.0D;
                translate[2] = 0.5D;
                break;
            case SOUTH:
                rotation = 3.0F;
                translate[0] = 0.5D;
                translate[1] = 1.0D;
                translate[2] = 0.6D;
                break;
            case EAST:
                rotation = 0.0F;
                translate[0] = 0.6D;
                translate[1] = 1.0D;
                translate[2] = 0.5D;
                break;
            default:
                rotation = 1.0F;
                translate[0] = 0.5D;
                translate[1] = 1.0D;
                translate[2] = 0.4D;
            }
            matrixStack.translate(translate[0], translate[1], translate[2]);
            matrixStack.multiply(Vector3f.POSITIVE_Y.getRadialQuaternion((float) Math.PI * rotation / 2.0F));
            matrixStack.multiply(Vector3f.POSITIVE_X.getRadialQuaternion((float) Math.PI));
            matrixStack.multiply(Vector3f.POSITIVE_Z.getRadialQuaternion((float) -Math.PI / 2.67F));
            matrixStack.scale(0.75F, 0.75F, 0.75F);
            VertexConsumer vertexBuilder = buffer
                    .getBuffer(this.coverModel.getLayer(Resources.LINKING_BOOK_ENTITY_TEXTURE));
            this.coverModel.render(matrixStack, vertexBuilder, arg4, arg5, this.color[0], this.color[1], this.color[2],
                    1.0F);
            this.pagesModel.render(matrixStack, vertexBuilder, arg4, arg5, 1.0F, 1.0F, 1.0F, 1.0F);

            matrixStack.pop();

        }
    }

}
