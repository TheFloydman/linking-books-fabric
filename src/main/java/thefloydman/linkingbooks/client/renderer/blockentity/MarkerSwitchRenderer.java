/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.client.renderer.blockentity;

import net.minecraft.block.BlockState;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;
import thefloydman.linkingbooks.block.MarkerSwitchBlock;
import thefloydman.linkingbooks.block.entity.MarkerSwitchBlockEntity;

public class MarkerSwitchRenderer extends BlockEntityRenderer<MarkerSwitchBlockEntity> {

    private final ItemRenderer itemRenderer;

    public MarkerSwitchRenderer(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
        this.itemRenderer = MinecraftClient.getInstance().getItemRenderer();
    }

    @Override
    public void render(MarkerSwitchBlockEntity blockEntity, float arg1, MatrixStack matrixStack,
            VertexConsumerProvider vertexConsumerProvider, int arg4, int arg5) {
        BlockState blockState = blockEntity.getWorld().getBlockState(blockEntity.getPos());
        if (blockState.getBlock() instanceof MarkerSwitchBlock) {
            if (blockState.get(MarkerSwitchBlock.OPEN) == true
                    && blockEntity.getWorld().getBlockState(blockEntity.getPos())
                            .get(MarkerSwitchBlock.HALF) == DoubleBlockHalf.LOWER
                    && !blockEntity.getStack(0).isEmpty()) {
                ItemStack itemStack = blockEntity.getStack(0);
                matrixStack.push();
                matrixStack.translate(0.5D, 0.5D, 0.5D);
                Direction facing = blockEntity.getCachedState().get(MarkerSwitchBlock.FACING);
                float rotation = facing == Direction.EAST || facing == Direction.WEST ? facing.asRotation() - 135.0F
                        : facing.asRotation() + 45.0F;
                matrixStack.multiply(Vector3f.POSITIVE_Y.getDegreesQuaternion(rotation));
                int lightAbove = WorldRenderer.getLightmapCoordinates(blockEntity.getWorld(),
                        blockEntity.getPos().up());
                this.itemRenderer.renderItem(itemStack, ModelTransformation.Mode.GROUND, lightAbove,
                        OverlayTexture.DEFAULT_UV, matrixStack, vertexConsumerProvider);
                matrixStack.pop();
            }
        }
    }

}