/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.world;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.PersistentState;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.util.registry.ModRegistries;

public class LinkingBooksPersistentState extends PersistentState {

    private Map<UUID, CompoundTag> linkingPanelImages = new HashMap<UUID, CompoundTag>();
    private Map<BlockPos, LinkDataComponent> linkingPortals = new HashMap<BlockPos, LinkDataComponent>();

    public LinkingBooksPersistentState(String key) {
        super(key);
    }

    public LinkingBooksPersistentState() {
        this(Reference.MOD_ID);
    }

    public boolean addLinkingPanelImage(UUID uuid, CompoundTag imageTag) {
        if (this.linkingPanelImages.containsKey(uuid)) {
            return false;
        }
        this.linkingPanelImages.put(uuid, imageTag);
        this.markDirty();
        return true;
    }

    public boolean removeLinkingPanelImage(UUID uuid) {
        if (!this.linkingPanelImages.containsKey(uuid)) {
            return false;
        }
        this.linkingPanelImages.remove(uuid);
        this.markDirty();
        return true;
    }

    public CompoundTag getLinkingPanelImage(UUID uuid) {
        return this.linkingPanelImages.get(uuid);
    }

    public boolean addLinkingPortalData(BlockPos pos, LinkDataComponent linkData) {
        this.linkingPortals.put(new BlockPos(pos), linkData);
        this.markDirty();
        return true;
    }

    public boolean removeLinkingPortalData(BlockPos pos) {
        if (!this.linkingPortals.containsKey(pos)) {
            return false;
        }
        this.linkingPortals.remove(pos);
        this.markDirty();
        return true;
    }

    public LinkDataComponent getLinkingPortalData(BlockPos pos) {
        return this.linkingPortals.get(pos);
    }

    @Override
    public void fromTag(CompoundTag tag) {
        if (tag.contains("linkingPanelImages", NbtType.LIST)) {
            ListTag list = tag.getList("linkingPanelImages", NbtType.COMPOUND);
            for (Tag item : list) {
                CompoundTag compound = (CompoundTag) item;
                if (compound.contains("uuid", NbtType.INT_ARRAY)) {
                    UUID uuid = compound.getUuid("uuid");
                    this.linkingPanelImages.put(uuid, compound);
                }
            }
        }
        if (tag.contains("linking_portals", NbtType.LIST)) {
            ListTag list = tag.getList("linking_portals", NbtType.COMPOUND);
            for (Tag item : list) {
                CompoundTag compound = (CompoundTag) item;
                BlockPos pos = NbtHelper.toBlockPos(compound.getCompound("portal_pos"));
                LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
                        .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
                linkData.setDimension(new Identifier(compound.getString("dimension")));
                linkData.setPosition(NbtHelper.toBlockPos(compound.getCompound("position")));
                linkData.setRotation(compound.getFloat("rotation"));
                linkData.setLinkEffects(compound.getList("link_effects", NbtType.STRING).stream().map((stringTag) -> {
                    return stringTag.asString();
                }).map(Identifier::new).map(LinkEffect::get).collect(Collectors.toSet()));
                linkData.setUUID(compound.getUuid("uuid"));
                this.linkingPortals.put(pos, linkData);
            }
        }
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        ListTag imageList = new ListTag();
        this.linkingPanelImages.forEach((uuid, compound) -> {
            compound.putUuid("uuid", uuid);
            imageList.add(compound);
        });
        tag.put("linkingPanelImages", imageList);
        ListTag portalList = new ListTag();
        this.linkingPortals.forEach((pos, linkData) -> {
            CompoundTag compound = new CompoundTag();
            compound.put("portal_pos", NbtHelper.fromBlockPos(pos));
            compound.putString("dimension", linkData.getDimension().toString());
            compound.put("position", NbtHelper.fromBlockPos(linkData.getPosition()));
            compound.putFloat("rotation", linkData.getRotation());
            ListTag linkEffects = new ListTag();
            linkEffects.addAll(linkData.getLinkEffects().stream().map(ModRegistries.LINK_EFFECTS::getId)
                    .map(Identifier::toString).map(StringTag::of).collect(Collectors.toList()));
            compound.put("link_effects", linkEffects);
            compound.putUuid("uuid", linkData.getUUID());
            portalList.add(compound);
        });
        tag.put("linking_portals", portalList);
        return tag;
    }

}
