/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.server.command;

import java.util.ArrayList;

import com.google.common.collect.Lists;
import com.mojang.brigadier.CommandDispatcher;

import net.minecraft.command.argument.BlockPosArgumentType;
import net.minecraft.command.argument.DimensionArgumentType;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.component.ModComponents;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.util.LinkingUtils;

public class LinkCommand {

    private static final String ENTITIES = "teleporting_entities";
    private static final String ENTITY = "destination_entity";
    private static final String POSITION = "position";
    private static final String DIMENSION = "dimension";

    public static void register(CommandDispatcher<ServerCommandSource> dispatcher, boolean isDedicatedServer) {

        dispatcher.register(CommandManager.literal("link").requires((context) -> {
            return context.hasPermissionLevel(2);
        })

                .then(CommandManager.argument(ENTITIES, EntityArgumentType.entities())
                        .then(CommandManager.argument(POSITION, BlockPosArgumentType.blockPos()).executes((context) -> {
                            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
                                    .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
                            linkData.setDimension(
                                    context.getSource().getPlayer().getEntityWorld().getRegistryKey().getValue());
                            linkData.setPosition(BlockPosArgumentType.getBlockPos(context, POSITION));
                            linkData.setRotation(context.getSource().getPlayer().yaw);
                            linkData.addLinkEffect(LinkEffects.INTRAAGE_LINKING);
                            return LinkingUtils.linkEntities(
                                    new ArrayList<>(EntityArgumentType.getEntities(context, ENTITIES)), linkData,
                                    false);
                        })))

                .then(CommandManager.argument(ENTITIES, EntityArgumentType.entities()).then(CommandManager
                        .argument(DIMENSION, DimensionArgumentType.dimension())
                        .then(CommandManager.argument(POSITION, BlockPosArgumentType.blockPos()).executes((context) -> {
                            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
                                    .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
                            linkData.setDimension(DimensionArgumentType.getDimensionArgument(context, DIMENSION)
                                    .getRegistryKey().getValue());
                            linkData.setPosition(BlockPosArgumentType.getBlockPos(context, POSITION));
                            linkData.setRotation(context.getSource().getPlayer().yaw);
                            linkData.addLinkEffect(LinkEffects.INTRAAGE_LINKING);
                            return LinkingUtils.linkEntities(
                                    new ArrayList<>(EntityArgumentType.getEntities(context, ENTITIES)), linkData,
                                    false);
                        }))))

                .then(CommandManager.argument(DIMENSION, DimensionArgumentType.dimension())
                        .then(CommandManager.argument(POSITION, BlockPosArgumentType.blockPos()).executes((context) -> {
                            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
                                    .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
                            linkData.setDimension(DimensionArgumentType.getDimensionArgument(context, DIMENSION)
                                    .getRegistryKey().getValue());
                            linkData.setPosition(BlockPosArgumentType.getBlockPos(context, POSITION));
                            linkData.setRotation(context.getSource().getPlayer().yaw);
                            linkData.addLinkEffect(LinkEffects.INTRAAGE_LINKING);
                            return LinkingUtils.linkEntities(
                                    new ArrayList<>(Lists.newArrayList(context.getSource().getPlayer())), linkData,
                                    false);
                        })))

                .then(CommandManager.argument(POSITION, BlockPosArgumentType.blockPos()).executes((context) -> {
                    LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
                            .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
                    linkData.setDimension(context.getSource().getPlayer().getEntityWorld().getRegistryKey().getValue());
                    linkData.setPosition(BlockPosArgumentType.getBlockPos(context, POSITION));
                    linkData.setRotation(context.getSource().getPlayer().yaw);
                    linkData.addLinkEffect(LinkEffects.INTRAAGE_LINKING);
                    return LinkingUtils.linkEntities(
                            new ArrayList<>(Lists.newArrayList(context.getSource().getEntityOrThrow())), linkData,
                            false);
                }))

                .then(CommandManager.argument(ENTITIES, EntityArgumentType.entities())
                        .then(CommandManager.argument(ENTITY, EntityArgumentType.entity()).executes((context) -> {
                            LinkDataComponent linkData = ModComponents.ITEM_LINK_DATA
                                    .get(ModItems.WRITTEN_LINKING_BOOK.getDefaultStack());
                            linkData.setDimension(EntityArgumentType.getEntity(context, ENTITY).getEntityWorld()
                                    .getRegistryKey().getValue());
                            linkData.setPosition(EntityArgumentType.getEntity(context, ENTITY).getBlockPos());
                            linkData.addLinkEffect(LinkEffects.INTRAAGE_LINKING);
                            int i = 0;
                            for (Entity entity : EntityArgumentType.getEntities(context, ENTITIES)) {
                                linkData.setRotation(entity.yaw);
                                i += LinkingUtils.linkEntities(new ArrayList<>(Lists.newArrayList(entity)), linkData,
                                        false);
                            }
                            return i;
                        })))

        );
    }
}
