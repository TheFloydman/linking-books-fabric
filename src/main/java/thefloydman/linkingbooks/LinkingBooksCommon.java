/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.serializer.Toml4jConfigSerializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import thefloydman.linkingbooks.block.ModBlocks;
import thefloydman.linkingbooks.block.entity.ModBlockEntityTypes;
import thefloydman.linkingbooks.config.ModConfig;
import thefloydman.linkingbooks.entity.ModEntityTypes;
import thefloydman.linkingbooks.event.EntityJoinWorldCallback;
import thefloydman.linkingbooks.event.ModEventHandlers;
import thefloydman.linkingbooks.event.PlayerTossItemCallback;
import thefloydman.linkingbooks.item.ModItems;
import thefloydman.linkingbooks.linking.LinkEffects;
import thefloydman.linkingbooks.recipe.ModRecipeSerializers;
import thefloydman.linkingbooks.screen.ModScreenHandlerTypes;
import thefloydman.linkingbooks.server.command.LinkCommand;

public class LinkingBooksCommon implements ModInitializer {

    @Override
    public void onInitialize() {
        AutoConfig.register(ModConfig.class, Toml4jConfigSerializer::new);
        ModBlocks.register();
        ModItems.register();
        ModRecipeSerializers.register();
        ModBlockEntityTypes.register();
        ModEntityTypes.register();
        ModScreenHandlerTypes.register();
        LinkEffects.register();

        // Register event handlers.
        PlayerTossItemCallback.EVENT.register(ModEventHandlers::playerTossItem);
        UseBlockCallback.EVENT.register(ModEventHandlers::playerUseBlock);
        EntityJoinWorldCallback.EVENT.register(ModEventHandlers::entityJoinWorld);
        ServerLifecycleEvents.SERVER_STARTED.register(ModEventHandlers::serverStarted);

        // Register commands.
        CommandRegistrationCallback.EVENT.register(LinkCommand::register);
    }
}
