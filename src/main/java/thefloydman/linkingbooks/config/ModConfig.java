/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.config;

import io.github.prospector.modmenu.api.ConfigScreenFactory;
import io.github.prospector.modmenu.api.ModMenuApi;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.ConfigData;
import me.sargunvohra.mcmods.autoconfig1u.annotation.Config;
import me.sargunvohra.mcmods.autoconfig1u.annotation.ConfigEntry;
import thefloydman.linkingbooks.util.Reference;

@Config(name = Reference.MOD_ID)
public class ModConfig implements ConfigData, ModMenuApi {

    @ConfigEntry.Gui.Tooltip(count = 3)
    public int linkingCostExperienceLevels = 0;
    @ConfigEntry.Gui.Tooltip(count = 2)
    public int linkingPanelChunkLoadRadius = 4;
    @ConfigEntry.Gui.Tooltip(count = 2)
    public int linkingPanelChunkRenderDistance = 4;
    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean useImmersivePortalsForLinkingPanels = true;
    @ConfigEntry.Gui.Tooltip(count = 2)
    public boolean useImmersivePortalsForLinkingPortals = true;

    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> AutoConfig.getConfigScreen(ModConfig.class, parent).get();
    }

}
