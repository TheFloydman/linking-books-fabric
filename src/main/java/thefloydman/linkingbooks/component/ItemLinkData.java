/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.component;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import dev.onyxstudios.cca.api.v3.item.ItemComponent;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtHelper;
import net.minecraft.nbt.StringTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import thefloydman.linkingbooks.api.component.LinkDataComponent;
import thefloydman.linkingbooks.api.linking.LinkEffect;
import thefloydman.linkingbooks.util.Reference;
import thefloydman.linkingbooks.util.registry.ModRegistries;

public class ItemLinkData extends ItemComponent implements LinkDataComponent {

    private static final String DIMENSION = "dimension";
    private static final String POSITION = "position";
    private static final String ROTATION = "rotation";
    private static final String UUID_LABEL = "uuid";
    private static final String LINK_EFFECTS = "link_effects";

    public ItemLinkData(ItemStack stack) {
        super(stack);
    }

    @Override
    public void setDimension(Identifier dimension) {
        this.putString(DIMENSION, dimension.toString());
    }

    @Override
    public Identifier getDimension() {
        if (!this.hasTag(DIMENSION, NbtType.STRING)) {
            this.putString(DIMENSION, "minecraft:overworld");
        }
        return new Identifier(this.getString(DIMENSION));
    }

    @Override
    public void setPosition(BlockPos pos) {
        this.putCompound(POSITION, NbtHelper.fromBlockPos(pos));
    }

    @Override
    public BlockPos getPosition() {
        if (!this.hasTag(POSITION, NbtType.COMPOUND)) {
            this.putCompound(POSITION, NbtHelper.fromBlockPos(
                    Reference.server == null ? BlockPos.ORIGIN : Reference.server.getOverworld().getSpawnPos()));
        }
        return NbtHelper.toBlockPos(this.getCompound(POSITION));
    }

    @Override
    public void setRotation(float rotation) {
        this.putFloat(ROTATION, rotation);
    }

    @Override
    public float getRotation() {
        if (!this.hasTag(ROTATION, NbtType.FLOAT)) {
            this.putFloat(ROTATION, 0.0F);
        }
        return this.getFloat(ROTATION);
    }

    @Override
    public void setUUID(UUID uuid) {
        this.putUuid(UUID_LABEL, uuid);
    }

    @Override
    public UUID getUUID() {
        if (!this.hasTag(UUID_LABEL, NbtType.INT_ARRAY)) {
            this.putUuid(UUID_LABEL, UUID.randomUUID());
        }
        return this.getUuid(UUID_LABEL);
    }

    @Override
    public void setLinkEffects(Set<LinkEffect> effects) {
        ListTag list = new ListTag();
        list.addAll(effects.stream().map(ModRegistries.LINK_EFFECTS::getId).map(Identifier::toString).map(StringTag::of)
                .collect(Collectors.toList()));
        this.putList(LINK_EFFECTS, list);
    }

    @Override
    public Set<LinkEffect> getLinkEffects() {
        if (!this.hasTag(LINK_EFFECTS, NbtType.LIST)) {
            this.putList(LINK_EFFECTS, new ListTag());
        }
        ListTag list = this.getList(LINK_EFFECTS, NbtType.STRING);

        Set<LinkEffect> effectSet = list.stream().map((stringTag) -> {
            return stringTag.asString();
        }).map(Identifier::new).map(LinkEffect::get).collect(Collectors.toSet());

        return effectSet;
    }

    @Override
    public boolean addLinkEffect(LinkEffect effect) {
        Set<LinkEffect> effectSet = this.getLinkEffects();
        if (effectSet.add(effect)) {
            this.setLinkEffects(effectSet);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeLinkEffect(LinkEffect effect) {
        Set<LinkEffect> effectSet = this.getLinkEffects();
        if (effectSet.remove(effect)) {
            this.setLinkEffects(effectSet);
            return true;
        }
        return false;
    }

    @Override
    public PacketByteBuf writeToPacketByteBuf(PacketByteBuf buffer) {
        buffer.writeCompoundTag(this.getRootTag());
        return buffer;
    }

    @Override
    public void readFromPacketByteBuf(PacketByteBuf buffer) {
        this.getOrCreateRootTag().copyFrom(buffer.readCompoundTag());
    }

    /**
     * @see CompoundTag#putUuid(String, UUID)
     */
    protected void putUuid(String key, UUID uuid) {
        if (uuid != null) {
            this.getOrCreateRootTag().putUuid(key, uuid);
        } else {
            this.remove(key);
        }
    }

}
