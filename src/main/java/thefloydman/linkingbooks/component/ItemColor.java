/*******************************************************************************
 * Linking Books - Fabric
 * Copyright (C) 2021  TheFloydman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can reach TheFloydman on Discord at Floydman#7171.
 *******************************************************************************/
package thefloydman.linkingbooks.component;

import dev.onyxstudios.cca.api.v3.item.ItemComponent;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DyeColor;
import thefloydman.linkingbooks.api.component.ColorComponent;
import thefloydman.linkingbooks.util.ColorUtils;

public class ItemColor extends ItemComponent implements ColorComponent {

    private int color = ColorUtils.dyeColorAsInt(DyeColor.GREEN);
    private static final String COLOR = "color";

    public ItemColor(ItemStack stack) {
        super(stack);
    }

    @Override
    public void setColor(int color) {
        this.putInt(COLOR, color);
    }

    @Override
    public int getColor() {
        if (!this.hasTag(COLOR, NbtType.INT))
            this.putInt(COLOR, this.color);
        return this.getInt(COLOR);
    }

}
